
var base_path_rpab = "https://zestardshop.com/shopifyapp/related-products-and-blogs/";
//var ztpl_related_product = jQuery.noConflict();
var ztpl_related_product = jQuery;
loadCSS = function (href) {
    var cssLink = ztpl_related_product("<link rel='stylesheet' type='text/css' href='" + href + "'>");
    ztpl_related_product("head").append(cssLink);
};
loadJS = function (src) {
    var jsLink = ztpl_related_product("<script src='" + src + "'></script>");
    ztpl_related_product("head").append(jsLink);
};
ztpl_related_product(document).ready(function () {

    loadCSS(base_path_rpab + "public/js/owl/owl.carousel.css");
    loadCSS(base_path_rpab + "public/js/owl/owl.theme.default.min.css");
    loadCSS(base_path_rpab + "public/css/related-products-and-blogs.css");
    loadJS(base_path_rpab + "public/js/owl/owl.carousel.js");
    //loadCSS(base_path_rpab + "public/css/mdb/font-awesome.min_463.css");

    if (typeof jQuery == 'undefined')
    {
        (function () {
            var jscript = document.createElement("script");
            jscript.src = base_path_rpab + "public/js/jquery-3.3.1.js";
            jscript.type = 'text/javascript';
            jscript.async = true;
            document.getElementsByTagName("head")[0].appendChild(jscript);
            //document.getElementById('datepicker_box_jquery').append(jscript);
        })(ztpl_related_product);

        if (typeof jQuery.ui == 'undefined') {

            var script = document.createElement('script');
            script.type = "text/javascript";
            script.src = base_path_rpab + 'public/js/jquery-ui.js';
            document.getElementsByTagName("head")[0].appendChild(script);
        }
        related_producrs_and_blogs();

    } else {
        if (typeof jQuery.ui == 'undefined') {

            var script = document.createElement('script');
            script.type = "text/javascript";
            script.src = base_path_rpab + 'public/js/jquery-ui.js';
            document.getElementsByTagName("head")[0].appendChild(script);
        }
        related_producrs_and_blogs();
    }

    //related_producrs_and_blogs();

});


function related_producrs_and_blogs() {

    var ProductlistDiv = ztpl_related_product('.zestard-productlist').length;
    var BloglistDiv = ztpl_related_product('.zestard-bloglist').length;
    var RandomProductlistDiv = ztpl_related_product('.zestard-randomproductlist').length;

    if (ProductlistDiv > 0)
    {
        var store_id = ztpl_related_product('.zestard-productlist').attr("store_id");
        var blog_id = ztpl_related_product('.zestard-productlist').attr("blog_id");
        if (store_id != '' && blog_id != '') {
            ztpl_related_product.ajax({
                type: "POST",
                url: base_path_rpab + "public/product_displays",
                crossDomain: true,
                data: {store_id: store_id, blog_id: blog_id},
                success: function (data) {
                    //debugger; 
                    var JSONParse = JSON.parse(data);
                    var pdata = JSONParse[0];
                    var itm = parseInt(JSONParse[1]);
                    var hide_css = parseInt(JSONParse[2]);
                    var app_status = parseInt(JSONParse[3]);
                    if (app_status == '1') {
                        ztpl_related_product(".zestard-productlist").html(pdata);
                        var owl = ztpl_related_product('.owl-carousel');
                        if (hide_css == 1) {
                            owl.owlCarousel({
                                margin: 20,
                                items: itm,
                                nav: true,
                                navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>'],
                                responsiveClass: true,
                                responsive: {
                                    0: {
                                        items: 1,
                                        nav: true
                                    },
                                    600: {
                                        items: 2,
                                        nav: true
                                    },
                                    1000: {
                                        items: itm,
                                        nav: true,
                                    }
                                }
                            });
                        } else if (hide_css == 2) {
                            owl.owlCarousel({
                                margin: 10,
                                items: 1,
                                nav: true,
                                //navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                                navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>'],
                                mergeFit: true,
                            });
                        }
                        //ztpl_related_product(".caption").removeAttr("style");
                    } //appstatus
                }
            });

        }


    } else if (BloglistDiv > 0) {

        var store_id = ztpl_related_product('.zestard-bloglist').attr("store_id");
        var product_id = ztpl_related_product('.zestard-bloglist').attr("product_id");
        if (store_id != '' && product_id != '') {

            ztpl_related_product.ajax({
                type: "POST",
                url: base_path_rpab + "public/blog_displays",
                crossDomain: true,
                data: {store_id: store_id, product_id: product_id},
                success: function (data) {
                    var JSONParse = JSON.parse(data);
                    var pdata = JSONParse[0];
                    var itm = parseInt(JSONParse[1]);
                    var hide_css = parseInt(JSONParse[2]);
                    var app_status = parseInt(JSONParse[3]);
                    if (app_status == '1') {

                        ztpl_related_product(".zestard-bloglist").html(pdata);
                        var owl = ztpl_related_product('.owl-carousel');
                        if (hide_css == 1) {
                            owl.owlCarousel({
                                margin: 20,
                                items: itm,
                                nav: true,
                                //navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                                navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>'],
                                responsiveClass: true,
                                responsive: {
                                    0: {
                                        items: 1,
                                        nav: true
                                    },
                                    600: {
                                        items: 2,
                                        nav: true
                                    },
                                    1000: {
                                        items: itm,
                                        nav: true,
                                    }
                                }
                            });
                        } else if (hide_css == 2) {
                            owl.owlCarousel({
                                margin: 10,
                                items: 1,
                                nav: true,
                                //navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                                navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>'],
                            });

                        }
                    } //appstatus  
                }
            });
        }
    } else if (RandomProductlistDiv > 0) {

        var store_id = ztpl_related_product('.zestard-randomproductlist').attr("store_id");
        var blog_id = ztpl_related_product('.zestard-randomproductlist').attr("blog_id");
        if (store_id != '' && blog_id != '') {
            ztpl_related_product.ajax({
                type: "POST",
                url: base_path_rpab + "public/randomproduct_displays",
                crossDomain: true,
                data: {store_id: store_id, blog_id: blog_id},
                success: function (data) {
                    //debugger; 
                    var JSONParse = JSON.parse(data);
                    var pdata = JSONParse[0];
                    var itm = parseInt(JSONParse[1]);
                    var hide_css = parseInt(JSONParse[2]);
                    var app_status = parseInt(JSONParse[3]);
                    if (app_status == '1') {

                        ztpl_related_product(".zestard-randomproductlist").html(pdata);
                        var owl = ztpl_related_product('.owl-carousel');
                        if (hide_css == 1) {
                            owl.owlCarousel({
                                margin: 20,
                                items: itm,
                                nav: true,
                                //navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                                navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>'],
                                responsiveClass: true,
                                responsive: {
                                    0: {
                                        items: 1,
                                        nav: true
                                    },
                                    600: {
                                        items: 2,
                                        nav: true
                                    },
                                    1000: {
                                        items: itm,
                                        nav: true,
                                    }
                                }
                            });
                        } else if (hide_css == 2) {
                            owl.owlCarousel({
                                margin: 10,
                                items: 1,
                                nav: true,
                                //navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
                                navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>'],
                                mergeFit: true,
                            });
                        }
                    } //appstatus    
                }
            });

        }
    }

}