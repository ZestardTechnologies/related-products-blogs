var base_path_rpab = "https://zestardshop.com/shopifyapp/related-products-and-blogs/";
var ztpl_related_product = jQuery;
loadCSS = function(href) {
    var cssLink = ztpl_related_product("<link rel='stylesheet' type='text/css' href='" + href + "'>");
    ztpl_related_product("head").append(cssLink);
};
loadJS = function(src) {
    var jsLink = ztpl_related_product("<script src='" + src + "'></script>");
    ztpl_related_product("head").append(jsLink);
};
ztpl_related_product(document).ready(function() {
    loadCSS(base_path_rpab + "public/js/owl/owl.carousel.css");
    loadCSS(base_path_rpab + "public/js/owl/owl.theme.default.min.css");
    loadCSS(base_path_rpab + "public/css/related-products-and-blogs.css");
    loadJS(base_path_rpab + "public/js/owl/owl.carousel.js");

    var product_id = ztpl_related_product('.zestard-bloglist').attr('product_id');
    var lang = ztpl_related_product('.zestard-bloglist').attr('current_language');
    var shop_name = Shopify.shop;
    var productData = "";
    var itm = "";
    //console.log(lang);
    ztpl_related_product.ajax({
        //async: false,
        type: "GET",
        url: base_path_rpab + "public/get_related_blog_langify",
        data: {
            product_id: product_id,
            shop_name: shop_name,
            language: lang
        },
        success: function(data) {
            //debugger;
            if (data.length > 0) {
                ztpl_related_product('#ztpl_content_loader').remove();
                var settings_data = JSON.parse(data);
                var general_settings = settings_data[0];
                var blog_data = settings_data[1];
                //console.log(settings_data[2].blog_page_title_fr);
                itm = general_settings.number_of_blogs_per_row;
                //alert(itm);
                if (parseInt(general_settings.app_status) == 1) {
                    if (general_settings.length > 0) {

                        if (general_settings.additional_css.length > 0) {
                            ztpl_related_product(".zestard-bloglist").after('<style>' + general_settings.additional_css + '</style>');
                        }

                    }
                    var slider_type = general_settings.slider_type;
                    if (parseInt(slider_type) == 1) {
                        ztpl_related_product("#ztpl_content_div").addClass("full-width-slider");
                    }
                    if (parseInt(slider_type) == 2) {
                        ztpl_related_product("#ztpl_content_div").addClass("sidebar-slider");
                    }
                    if (parseInt(slider_type) == 3) {
                        ztpl_related_product("#ztpl_content_div").addClass("full-width-list");
                    }
                    if (parseInt(slider_type) == 4) {
                        ztpl_related_product("#ztpl_content_div").addClass("sidebar-list");
                    }

                    //for setting title
                    if (lang == "ly59047") {
                        ztpl_related_product(".blog_page_title").append(settings_data[2].blog_page_title_fr);
                    } else {
                        ztpl_related_product(".blog_page_title").append(general_settings.blog_page_title);
                    }


                    if (blog_data.length > 0) {
                        //ztpl_related_product("#ztpl_content_div").html("")
                        var i = 0;
                        ztpl_related_product(blog_data).each(function(k, v) {
                            // alert(v.handle);
                            //console.log(v.description);
                            blogslideData = '<article class="thumbnail item">' +
                                '<a class="blog-thumb-img" href="https://www.maillard.co/blogs/' + blog_data[i].handle + '" title=""><img src="' + blog_data[i].image + '" class="img-responsive"></a>' +
                                '<div class="caption">' +
                                '<h4 itemprop="headline">' +
                                '<a href="https://www.maillard.co/blogs/' + blog_data[i].handle + '" rel="bookmark">' + blog_data[i].title + '</a>' +
                                '</h4>' +
                                '<p itemprop="text" class="flex-text text-muted">' + blog_data[i].description + '</p>' +
                                '</div>' +
                                '</article>';

                            ztpl_related_product("#ztpl_content_div").append(blogslideData);
                            //console.log(blogslideData);
                            //alert(blogslideData);
                            i = i + 1;
                        });
                    }
                    //console.log(products_ids);
                    //alert("test");
                    load_product_slider(itm);

                }



            } else {
                ztpl_related_product('#ztpl_content_loader').remove();
            }

        }

    });

});

function load_product_slider(itm) {
    //alert(itm);
    //console.log(itm);
    //var owl = $('.owl-carousel');
    //debugger;
    ztpl_related_product('.owl-carousel').owlCarousel({
        margin: 20,
        autoWidth: true,
        items: itm,
        nav: true,
        navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g><path style="fill:#010002;" d="M10.544,11.031l6.742-6.742c0.81-0.809,0.81-2.135,0-2.944l-0.737-0.737 c-0.81-0.811-2.135-0.811-2.945,0L4.769,9.443c-0.435,0.434-0.628,1.017-0.597,1.589c-0.031,0.571,0.162,1.154,0.597,1.588 l8.835,8.834c0.81,0.811,2.135,0.811,2.945,0l0.737-0.737c0.81-0.808,0.81-2.134,0-2.943L10.544,11.031z"/></g></svg>', '<svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 22.062 22.062" style="enable-background:new 0 0 22.062 22.062;" xml:space="preserve"><g> <path d="M17.294,9.443L8.459,0.608c-0.812-0.811-2.138-0.811-2.945,0L4.775,1.345c-0.81,0.81-0.81,2.136,0,2.944l6.742,6.743 l-6.742,6.741c-0.81,0.811-0.81,2.137,0,2.943l0.737,0.737c0.81,0.811,2.136,0.811,2.945,0l8.835-8.836  c0.435-0.435,0.628-1.018,0.597-1.589C17.922,10.46,17.729,9.877,17.294,9.443z"/></g></svg>']
    });
}