<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::any('change_currency', 'callbackController@Currency')->name('change_currency');

Route::post('search', function () {
    return view('search');
})->middleware('cors')->name('search');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('help', function () {
    return view('help');
})->name('help');

Route::get('frontend', 'FrontendController@frontend')->middleware('cors')->name('frontend');

Route::get('customerlist', 'CustomerController@index')->name('customerlist');

Route::get('edit_customer/{id}', 'CustomerController@edit')->name('edit_customer');

Route::any('productlist', 'ProductController@index')->name('productlist');

Route::get('getproducts', 'ProductController@get_products')->name('getproducts');

Route::get('getblogs', 'BlogController@get_blogs')->name('getblogs');

Route::any('get_productlist_for_editblogs', 'BlogController@get_productlist_for_editblogs')->name('get_productlist_for_editblogs');

Route::any('get_bloglist_for_editproducts', 'ProductController@get_bloglist_for_editproducts')->name('get_bloglist_for_editproducts');

Route::get('edit_product/{id}', 'ProductController@edit')->name('edit_product');

Route::any('product/store', 'ProductController@store')->name('store_product');

Route::post('product/search', 'ProductController@search');

Route::get('getcustomers', 'CustomerController@get_customers')->name('getcustomers');

Route::any('bloglist', 'BlogController@index');

Route::get('blog/create', 'BlogController@create');

Route::any('blog/store', 'BlogController@store')->name('store_blog');

Route::post('blog/store', 'BlogController@store');

//Route::get('edit_blog/{id}', 'BlogController@edit');
Route::get('edit_blog/{blogPostid}/articles/{blogid}', 'BlogController@edit')->name('edit_blog');

Route::any('blog/update/{id}', 'BlogController@update')->middleware('cors');

Route::get('delete_blog/{id}', 'BlogController@delete');

Route::get('product/import', 'ImportProductController@index');

Route::get('generalsettings', 'GeneralSettingController@index')->name('generalsettings');

Route::post('setting/store', 'GeneralSettingController@store');

Route::get('help', 'HelpController@index')->name('help');

Route::any('product_displays', 'FrontendController@Productindex')->middleware('cors')->name('product_displays');

Route::any('blog_displays', 'FrontendController@Blogindex')->middleware('cors')->name('blog_displays');

Route::any('randomproduct_displays', 'FrontendController@RandomProductindex')->middleware('cors')->name('randomproduct_displays');

Route::get('declined', 'callbackController@declined')->name('declined');

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status'); 

Route::get('decline', function () {
    return view('decline');
})->name('decline');

//to create blog 240
//Route::any('create_blog', 'CreateblogController@index')->name('create_blog');

//route for mailyard langify
Route::any('get_related_products_langify', 'FrontendController@get_related_product_list_langify_compatible')->middleware('cors')->name('get_related_products_langify');
Route::any('get_related_blog_langify', 'FrontendController@get_related_blog_list_langify_compatible')->middleware('cors')->name('get_related_blog_langify');

Route::any('BlogRefresh', 'GeneralSettingController@refresh_blog')->name('BlogRefresh');
Route::any('ProductRefresh', 'GeneralSettingController@refresh_product')->name('ProductRefresh');