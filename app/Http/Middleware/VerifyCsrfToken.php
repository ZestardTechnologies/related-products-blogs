<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'generalsettings','product_displays','blog_displays','randomproduct_displays','get_productlist_for_editblogs','get_bloglist_for_editproducts','store_product','product/store','productlist','update_blog','blog/update/{id}','get_related_blog_langify'
    ];
}
