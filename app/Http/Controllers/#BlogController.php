<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App;
use App\ShopModel;
use App\ProductBlog;

//use App\Blog;

class BlogController extends Controller {

    public function index(Request $request) {

        return view('blog');
    }

    public function get_blogs(Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $limit = $request['length'];
        $draw = $request['draw'];
        $start = $request['start'];

        $blog_list = $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'GET']);
        $total_pageCount = 0;
        $productcountArr = array();
        $display_blog = array();
        $total_blogs['data'] = "";
        $default_image = url('/image/default.png');

        $search = $request['search']['value'];
        if ($search) {

            foreach ($blog_list->blogs as $key => $blog) {
                $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                $blog_count = $blogPOST_countArr->count;
                //$current_page = ceil($start / $limit) + 1;

                $pages = ceil($blog_count / 250);
                $limit = 250;

                if ($blog_count > 0) {

                    for ($i = 0; $i < $pages; $i++) {
                        $current_page = $i + 1;
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                        $total_blogs = array('draw' => $draw, 'recordsTotal' => $blog_count, 'recordsFiltered' => $blog_count);
                        $val = 1;
                        foreach ($blogPOST_list->articles as $posts) {
                            if (stristr($posts->title, $search)) {
                                $product_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $posts->id . ""], ['status', '=', '2']])->get()->count();
                                if (isset($posts->image)) {
                                    $image = $posts->image->src;
                                } else {
                                    $image = $default_image;
                                }
                                $link = 'edit_blog/' . $posts->id . '/articles/' . $posts->blog_id;
                                //$total_blogs['data'][] = array($val, $posts->id, $image, $posts->title, $product_count, $link);
                                $total_blogs['data'][] = array($val, $image, $posts->title, $product_count, $link);
                                $val++;
                            }
                        }
                    }
                }
            }
        } else {

            foreach ($blog_list->blogs as $key => $blog) {
                $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                $blog_count = $blogPOST_countArr->count;
                $current_page = ceil($start / $limit) + 1;

                if ($blog_count > 0) {

                    $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                    $total_blogs = array('draw' => $draw, 'recordsTotal' => $blog_count, 'recordsFiltered' => $blog_count);
                    $val = 1;
                    foreach ($blogPOST_list->articles as $posts) {
                        $product_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $posts->id . ""], ['status', '=', '2']])->get()->count();
                        if (isset($posts->image)) {
                            $image = $posts->image->src;
                        } else {
                            $image = $default_image;
                        }
                        $link = 'edit_blog/' . $posts->id . '/articles/' . $posts->blog_id;
                        //$total_blogs['data'][] = array($val, $posts->id, $image, $posts->title, $product_count, $link);
                        $total_blogs['data'][] = array($val, $image, $posts->title, $product_count, $link);
                        $val++;
                    }
                }
            }
        }
        return json_encode($total_blogs);
    }

    public function get_productlist_for_editblogs(Request $request) {

        $blogid = $request['blogid'];
        $post_blog_id = $request['post_blog_id'];

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $count = $sh->call(['URL' => '/admin/products/count.json', 'METHOD' => 'GET']);
        $products_count = (array) $count;
        $product_count = $products_count['count'];

        $limit = $request['length'];
        $draw = $request['draw'];
        $start = $request['start'];
        $current_page = ceil($start / $limit) + 1;
        $total_products = array('draw' => $draw, 'recordsTotal' => $product_count, 'recordsFiltered' => $product_count);
        $search = $request['search']['value'];
        $default_image = url('/image/default.png');
        if ($search) {
            $pages = ceil($product_count / 250);
            $limit = 250;
            for ($i = 0; $i < $pages; $i++) {
                $current_page = $i + 1;
                $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                $product_data = array($product_list);
                $val = $start + 1;
                $product_blogDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['blogid', '=', $blogid], ['status', '=', '2']])->pluck('productid')->toArray();

                foreach ($product_data as $productdata) {
                    foreach ($productdata as $products) {
                        foreach ($products as $key => $product) {

                            $checkedval = (in_array($product->id, $product_blogDB)) ? 'checked' : '';
                            $checkbox = '<input type="checkbox" name="productid[]" value="' . $product->id . ',' . $product->handle . '" id="' . $product->id . '" class="form-check-input select-productcheckbox" ' . $checkedval . ' >';
                            if (stristr($product->title, $search)) {

                                if (isset($product->images[0])) {
                                    $image = $product->images[0]->src;
                                } else {
                                    $image = $default_image;
                                }

                                $total_products['data'][] = array($val, $checkbox,$image, $product->title);
                            }
                            $val++;
                        }
                    }
                }
            }
        } else {

            $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
            $product_data = array($product_list);
            $val = $start + 1;
            $product_blogDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['blogid', '=', $blogid], ['status', '=', '2']])->pluck('productid')->toArray();

            foreach ($product_data as $productdata) {
                foreach ($productdata as $products) {
                    foreach ($products as $key => $product) {

                        if (isset($product->images[0])) {
                            $image = $product->images[0]->src;
                        } else {
                            $image = $default_image;
                        }

                        $checkedval = (in_array($product->id, $product_blogDB)) ? 'checked' : '';
                        $checkbox = '<input type="checkbox" name="productid[]" value="' . $product->id . ',' . $product->handle . '"    id="' . $product->id . '" class="form-check-input select-productcheckbox" ' . $checkedval . ' >';
                        $total_products['data'][] = array($val, $checkbox,$image, $product->title);
                        $val++;
                    }
                }
            }
        }


        return json_encode($total_products);
    }

    public function create() {
        return view('create');
    }

    public function store(Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $blog_argument = [
            'blog' => [
                'title' => $request->Input('title')
            ]
        ];
        $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'POST', 'DATA' => $blog_argument]);
        return redirect('bloglist');
    }

    public function edit($blogPost, $blog_id) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog_id . '/articles/' . $blogPost . '.json ', 'METHOD' => 'GET']);
        $product_list = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'GET']);
        $product_data = array($product_list);
        $product_blogDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['blogid', '=', $blogPost], ['status', '=', '2']])->get(['productid', 'handle']);
        $product_detail_array = array();

        foreach ($product_blogDB as $product_data) {
            $product_detail_array[] = $product_data->productid . "," . $product_data->handle;
        }
        $product_blogDB = json_encode($product_detail_array);

        //echo "<pre>"; print_r($product_blogDB);die;

        return view('edit_blog', ['blog_data' => $blog_data, 'product_data' => $product_data, 'product_blogDB' => $product_blogDB]);
    }

    public function update($id, Request $request) {

        $input = $request->all();
        $multipleProductId = (isset($input['data'])) ? $input['data'] : 0;

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        //-----------
        //$multipleProductId = $request->productid;
        $deleteRecord = DB::table('blog_product')->where([
                    ['storeid', '=', $select_store[0]->id],
                    ['blogid', '=', $input['blogid']],
                    ['status', '=', '2'],
                ])->get();
        if (count($deleteRecord) > 0) {
            $deleteRecord = DB::table('blog_product')->where([
                        ['storeid', '=', $select_store[0]->id],
                        ['blogid', '=', $input['blogid']],
                        ['status', '=', '2'],
                    ])->delete();
        }

        if ($multipleProductId > 0) {

            foreach ($multipleProductId as $productids) {
                $ids = explode(",", $productids);
                $productid = $ids[0];
                $blog_handle = $ids[1];
                DB::table('blog_product')->insert(
                        ['blogid' => $input['blogid'], 'productid' => $productid, 'storeid' => $select_store[0]->id, 'post_blog_id' => $input['post_blog_id'], 'handle' => $blog_handle, 'status' => '2']
                );
            }
        }
        //----------- 
        //$request->session()->flash('alert-success', 'Your products are asigned to these blog!');
        $notification = array(
            'message' => 'Your products are asigned to these blog!',
            'alert-type' => 'success'
        );
        $blog_data = $sh->call(['URL' => '/admin/blogs/' . $input['post_blog_id'] . '/articles/' . $input['blogid'] . '.json ', 'METHOD' => 'GET']);
        $product_list = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'GET']);
        $product_data = array($product_list);
        $product_blogDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['blogid', '=', $input['blogid']], ['status', '=', '2']])->pluck('productid')->toArray();
        //return view('edit_blog', ['blog_data' => $blog_data, 'product_data' => $product_data, 'product_blogDB' => $product_blogDB]);
        return redirect('bloglist')->with('notification', $notification);
        ;
    }

    public function delete($id) {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $sh->call(['URL' => '/admin/blogs/' . $id . '.json', 'METHOD' => 'DELETE']);
        return redirect('bloglist');
    }

}
