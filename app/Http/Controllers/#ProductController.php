<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App;
use App\ShopModel;
use Carbon\Carbon;
use Redirect;
use App\ProductBlog;

class ProductController extends Controller {

    public function index(Request $request) {

        return view('product');
    }

    public function get_products(Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $count = $sh->call(['URL' => '/admin/products/count.json', 'METHOD' => 'GET']);
        $products_count = (array) $count;
        $product_count = $products_count['count'];
        $limit = $request['length'];
        $draw = $request['draw'];
        $start = $request['start'];
        $current_page = ceil($start / $limit) + 1;
        $default_image = url('/image/default.png');
        $search = $request['search']['value'];
        $total_products = array('draw' => $draw, 'recordsTotal' => $product_count, 'recordsFiltered' => $product_count);

        $val = $start + 1;
        if ($search) {
            $pages = ceil($product_count / 250);
            $limit = 250;
            for ($i = 0; $i < $pages; $i++) {
                $current_page = $i + 1;
                $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                foreach ($product_list->products as $product) {
                    $blog_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $product->id . ""], ['status', '=', '1']])->get()->count();

                    if (stristr($product->title, $search)) {
                        if (isset($product->images[0])) {
                            $image = $product->images[0]->src;
                        } else {
                            $image = $default_image;
                        }
                        $total_products['data'][] = array($val, $image, $product->title, $blog_count, $product->id);
                    }
                    $val++;
                }
            }
        } else {
            $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
            foreach ($product_list->products as $product) {
                if (isset($product->images[0])) {
                    $image = $product->images[0]->src;
                } else {
                    $image = $default_image;
                }
                $blog_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $product->id . ""], ['status', '=', '1']])->get()->count();
                $total_products['data'][] = array($val, $image, $product->title, $blog_count, $product->id);
                $val++;
            }
        }

        return json_encode($total_products);
    }

    public function get_bloglist_for_editproducts(Request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $limit = $request['length'];
        $draw = $request['draw'];
        $start = $request['start'];

        $blog_list = $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'GET']);
        $total_pageCount = 0;
        $productcountArr = array();
        $display_blog = array();
        $total_blogs['data'] = "";
        $default_image = url('/image/default.png');
        $productid = $request['productid'];
        $blog_productDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['productid', '=', $productid], ['status', '=', '1']])->pluck('blogid')->toArray();
        $search = $request['search']['value'];
        if ($search) {

            foreach ($blog_list->blogs as $key => $blog) {
                $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                $blog_count = $blogPOST_countArr->count;
                //$current_page = ceil($start / $limit) + 1;

                $pages = ceil($blog_count / 250);
                $limit = 250;

                if ($blog_count > 0) {

                    for ($i = 0; $i < $pages; $i++) {
                        $current_page = $i + 1;
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                        $total_blogs = array('draw' => $draw, 'recordsTotal' => $blog_count, 'recordsFiltered' => $blog_count);
                        $val = $start + 1;
                        foreach ($blogPOST_list->articles as $posts) {
                            if (stristr($posts->title, $search)) {
                                if (isset($posts->image)) {
                                    $image = $posts->image->src;
                                } else {
                                    $image = $default_image;
                                }
                                $checkedval = (in_array($posts->id, $blog_productDB)) ? 'checked' : '';
                                $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $posts->blog_id . '.json', 'METHOD' => 'GET']);
                                $handle = $blogPOST_list->blog->handle . "/" . $posts->handle;
                                $checkbox = '<input type="checkbox" name="blogid[]" value="' . $posts->id . ',' . $posts->blog_id . ',' . $handle . '" id="' . $posts->id . '" class="form-check-input select-blogcheckbox" ' . $checkedval . '>';
                                $total_blogs['data'][] = array($val, $checkbox, $image, $posts->title);
                                $val++;
                            }
                        }
                    }
                }
            }
        } else {

            foreach ($blog_list->blogs as $key => $blog) {
                $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                $blog_count = $blogPOST_countArr->count;
                $current_page = ceil($start / $limit) + 1;

                if ($blog_count > 0) {

                    $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                    $total_blogs = array('draw' => $draw, 'recordsTotal' => $blog_count, 'recordsFiltered' => $blog_count);
                    $val = $start + 1;
                    foreach ($blogPOST_list->articles as $posts) {
                        if (isset($posts->image)) {
                            $image = $posts->image->src;
                        } else {
                            $image = $default_image;
                        }
                        $checkedval = (in_array($posts->id, $blog_productDB)) ? 'checked' : '';
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $posts->blog_id . '.json', 'METHOD' => 'GET']);
                        $handle = $blogPOST_list->blog->handle . "/" . $posts->handle;
                        $checkbox = '<input type="checkbox" name="blogid[]" value="' . $posts->id . ',' . $posts->blog_id . ',' . $handle . '" id="' . $posts->id . '" class="form-check-input select-blogcheckbox" ' . $checkedval . '>';
                        $total_blogs['data'][] = array($val, $checkbox, $image, $posts->title);
                        $val++;
                    }                    
                }
            }
        }
        return json_encode($total_blogs);
    }

    public function edit($id) {

        $shop = session('shop');
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $product = $sh->call(['URL' => '/admin/products/' . $id . '.json', 'METHOD' => 'GET']);
        $blog_list = $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'GET']);
        $total_pageCount = 0;
        $display_blog = array();
        foreach ($blog_list->blogs as $key => $blog) {

            $blogPOST_count = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
            $total_blog = $blogPOST_count->count;
            $total_pageCount = $total_pageCount + $total_blog;

            if (($blogPOST_count->count) > 0) {
                $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=250', 'METHOD' => 'GET']);
                foreach ($blogPOST_list->articles as $posts) {
                    $display_blog[] = $posts;
                }
            }
        }
        //$blog_productDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['productid', '=', $id], ['status', '=', '1']])->pluck('blogid')->toArray();
        $blog_productDB_json = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['productid', '=', $id], ['status', '=', '1']])->get(['blogid', 'post_blog_id', 'handle']);


        $blog_detail_array = array();

        foreach ($blog_productDB_json as $data) {
            $blog_detail_array[] = $data->blogid . "," . $data->post_blog_id . "," . $data->handle;
        }
        $blog_productDB = json_encode($blog_detail_array);
        return view('editproduct', ['product_info' => $product, 'display_blog' => $display_blog, 'blog_productDB' => $blog_productDB, 'settings' => '', 'product_id' => $id, 'store' => $shop, 'active' => '']);
    }

    public function store(Request $request) {
        
        $shop = session('shop');
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        //$multipleBlogId = $request->blogid;
        $input = $request->all();
        $multipleBlogId = (isset($input['data'])) ? $input['data'] : 0;
        
        
        $deleteRecord = DB::table('blog_product')->where([
                    ['storeid', '=', $select_store[0]->id],
                    ['productid', '=', $input['productid']],
                    ['status', '=', '1'],
                ])->get();
        if (count($deleteRecord) > 0) {
            $deleteRecord = DB::table('blog_product')->where([
                        ['storeid', $select_store[0]->id],
                        ['productid', $input['productid']],
                        ['status', '1'],
                    ])->delete();
        }

        if ($multipleBlogId > 0) {

            foreach ($multipleBlogId as $blogids) {
                $ids = explode(",", $blogids);
                $blogid = $ids[0];
                $post_blog_id = $ids[1];
                $product_handel = $ids[2];
                //$blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $post_blog_id . '.json', 'METHOD' => 'GET']);
                //$handle = $blogPOST_list->blog->handle . "/" . $product_handel;
                DB::table('blog_product')->insert(
                        ['productid' => $input['productid'], 'blogid' => $blogid, 'storeid' => $select_store[0]->id, 'post_blog_id' => $post_blog_id, 'handle' => $product_handel, 'status' => '1']
                );
            }
        }
    }

    public function search(Request $request) {

        $search_blog = $request->search_blog;
        $product_id = $request->HDNproductid;
        $shop = session('shop');
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        
        $blog_info = $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'GET']);
        $blogs_collection = collect($blog_info->blogs);
        $blogs_result = array();
        foreach ($blog_info->blogs as $key => $blog) {
            $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json', 'METHOD' => 'GET']);
            foreach ($blogPOST_list->articles as $keyVal => $posts) {
                if (preg_match('/' . $search_blog . '/', $posts->title)) {
                    $blogs_result[] = $posts;
                }
            }
        }
        $blogs_collection_query = $blogs_collection->where('title', '=', $search_blog);
        $blogs_array = array();
        foreach ($blog_info->blogs as $blog) {
            $blogs_array[] = $blog->title;
        }
        $product = $sh->call(['URL' => '/admin/products/' . $product_id . '.json', 'METHOD' => 'GET']);
        $blog_productDB = DB::table('blog_product')->where([['storeid', '=', $select_store[0]->id], ['productid', '=', $product_id], ['status', '=', '1']])->pluck('blogid')->toArray();
        return view('searchblog', ['blogs_result' => $blogs_result, 'product_info' => $product, 'blog_info' => $blog_info, 'blog_productDB' => $blog_productDB, 'blogs_array' => $blogs_array, 'search_blog' => $search_blog, 'settings' => '', 'store' => $shop]);
    }

}
