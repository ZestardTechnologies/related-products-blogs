<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HelpController extends Controller {

    public function index(Request $request) {
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        
        return view('help',['usersettings' => $select_store]);
        
    }

    

}
