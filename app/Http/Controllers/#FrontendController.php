<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;
use App\ProductBlog;

class FrontendController extends Controller {

    public function Productindex(request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get();

        $number_of_products_per_row = $general_settings[0]->number_of_products_per_row;
        $slider_type = $general_settings[0]->slider_type;
        $app_status = $general_settings[0]->app_status;
        $ProductBlog_modal = new ProductBlog;
        $product_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->get()->count();
        $productData = '';

        $dispaly_randon_products_if_not_assign = $general_settings[0]->dispaly_randon_products_if_not_assign;
        
        
        if ($dispaly_randon_products_if_not_assign != '1') {

            if ($product_count > 0) {

                $productArr = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->get();

                $val = 1;
                if ($slider_type == '1') {
                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                    foreach ($productArr as $product) {

                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);

                        $URLlink = "https://" . $shop . "/products/" . $product->handle;

                        $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                        $val++;
                    }
                    $productData.= '</div>';
                } else if ($slider_type == '2') {
                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider">';
                    foreach ($productArr as $product) {
                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);
                        $URLlink = "https://" . $shop . "/products/" . $product->handle;

                        $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                        $val++;
                    }
                    $productData.= '</div>';
                } else if ($slider_type == '3') {

                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="full-width-listId" class="full-width-list">';
                    foreach ($productArr as $product) {
                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;
                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);

                        $URLlink = "https://" . $shop . "/products/" . $product->handle;
                        $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                        $val++;
                    }
                    $productData.= '</div>';
                } else if ($slider_type == '4') {
                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="sidebar-listId" class="sidebar-list">';
                    foreach ($productArr as $product) {
                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);

                        $URLlink = "https://" . $shop . "/products/" . $product->handle;
                        $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                        $val++;
                    }
                    $productData.= '</div>';
                }
            }
        } else {

            $limit = 5;
            $current_page = 1;
            $productArr = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
            $val = 1;
            if ($slider_type == '1') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;
                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '2') {

                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider">';
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;

                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/products/" . $product->handle;

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '3') {

                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="full-width-listId" class="full-width-list">';
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;
                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '4') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="sidebar-listId" class="sidebar-list">';
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;

                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            }
        }
        $product_data = array($productData, $number_of_products_per_row, $slider_type, $app_status,$store_encrypt);
        return json_encode($product_data);
    }

    public function Blogindex(request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get();
        $number_of_blogs_per_row = $general_settings[0]->number_of_blogs_per_row;
        $slider_type = $general_settings[0]->slider_type;
        $app_status = $general_settings[0]->app_status;
        $ProductBlog_modal = new ProductBlog;

        $blogData = '';

        $dispaly_randon_blogs_if_not_assign = $general_settings[0]->dispaly_randon_blogs_if_not_assign;

        if ($dispaly_randon_blogs_if_not_assign != '1') {

            $blog_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $request->product_id . ""], ['status', '=', '1']])->get()->count();
            if ($blog_count > 0) {
                $blogArr = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $request->product_id . ""], ['status', '=', '1']])->get();
            }

            $val = 1;
            if ($slider_type == '1') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                foreach ($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json ', 'METHOD' => 'GET']);

                    $default_image = url('/image/default.png');
                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    //$link = 'edit_blog/' . $posts->id . '/articles/' . $posts->blog_id;
                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blog_data->article->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            } else if ($slider_type == '2') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider" style="width:26.02%;height:600px;margin:0 auto;overflow:hidden;display:block;">';
                foreach ($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json ', 'METHOD' => 'GET']);

                    $default_image = url('/image/default.png');

                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blog_data->article->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            } else if ($slider_type == '3') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="full-width-listId" class="full-width-list">';
                foreach ($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json ', 'METHOD' => 'GET']);

                    $default_image = url('/image/default.png');

                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blog_data->article->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            } else if ($slider_type == '4') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="sidebar-listId" class="sidebar-list" style="width:26.02%;margin:0 auto;overflow:hidden;display:block;">';
                foreach ($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json ', 'METHOD' => 'GET']);

                    $default_image = url('/image/default.png');

                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blog_data->article->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            }
        } else {

            $val1 = 1;
            $blogArr = $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'GET']);
            foreach ($blogArr->blogs as $blog) {
                $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                $current_page = 1;
                $limit = 5;
                $blog_count = $blogPOST_countArr->count;                
                if ($blog_count > 0) {
                    $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                    if ($slider_type == '1') {
                        $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                        $blogData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                        foreach ($blogPOST_list->articles as $articles) {
                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" .$blog->handle . "/" . $articles->handle;
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $articles->title . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        } 
                        $blogData.= '</div>';
                    } else if ($slider_type == '2') {
                        $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                        $blogData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider" style="width:26.02%;height:600px;margin:0 auto;overflow:hidden;display:block;">';
                        foreach ($blogPOST_list->articles as $articles) {

                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" .$blog->handle . "/" . $articles->handle;
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $articles->title . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                        $blogData.= '</div>';
                    } else if ($slider_type == '3') {
                        $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                        $blogData .= '<div id="full-width-listId" class="full-width-list">';
                        foreach ($blogPOST_list->articles as $articles) {

                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" .$blog->handle . "/" . $articles->handle;
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $articles->title . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                        $blogData.= '</div>';
                    } else if ($slider_type == '4') {
                        $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                        $blogData .= '<div id="sidebar-listId" class="sidebar-list" style="width:26.02%;margin:0 auto;overflow:hidden;display:block;">';
                        foreach ($blogPOST_list->articles as $articles) {

                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" .$blog->handle . "/" . $articles->handle;
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $articles->title . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                        $blogData.= '</div>';
                    }
                }
            }
            //die;
        }

        $return_data = array($blogData, $number_of_blogs_per_row, $slider_type, $app_status,$store_encrypt);
        return json_encode($return_data);
    }

    public function RandomProductindex(request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get();



        $number_of_products_per_row = $general_settings[0]->number_of_products_per_row;
        $slider_type = $general_settings[0]->slider_type;
        $app_status = $general_settings[0]->app_status;
        $ProductBlog_modal = new ProductBlog;
        $product_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->get()->count();

        $productData = '';
        if ($product_count > 0) {
            $productArr = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->orderBy('id', 'DESC')->get();

            $val = 1;
            if ($slider_type == '1') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                foreach ($productArr as $product) {



                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');

                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;


                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '2') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider">';
                foreach ($productArr as $product) {
                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');

                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '3') {

                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="full-width-listId" class="full-width-list">';
                foreach ($productArr as $product) {
                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');
                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;
                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '4') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="sidebar-listId" class="sidebar-list">';
                foreach ($productArr as $product) {
                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');

                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            }
        }
        $product_data = array($productData, $number_of_products_per_row, $slider_type, $app_status,$store_encrypt);
        return json_encode($product_data);
    }

}
