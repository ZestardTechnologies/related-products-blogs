<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class GeneralSettingController extends Controller {

    public function index(Request $request) {

        //$shop = session('shop');
        $shop = $_REQUEST['shop'];
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        //echo "<pre>";print_r($select_store); die;
		$new_install = $select_store[0]->new_install;	
        $setting_exist = DB::table('general_settings')->where('storeid', $select_store[0]->id)->first();
        
        if (count($setting_exist) > 0) {
    
            if($shop == "maillard.myshopify.com"){ // for maillard langify title
                $setting_langify = DB::table('langify_translate_title')->where('store_name', $shop)->first();
                return view('general_setting', ['settingArr' => $setting_exist, 'usersettings' => $select_store, 'new_install' => $new_install, 'new_shortcode' => $select_store[0]->new_shortcode, 'langify_settings' => $setting_langify]);
            }else{
                return view('general_setting', ['settingArr' => $setting_exist, 'usersettings' => $select_store, 'new_install' => $new_install, 'new_shortcode' => $select_store[0]->new_shortcode]);
            }
            
        } else {
            return view('general_setting', ['settingArr' => "", 'usersettings' => $select_store, 'new_install' => $new_install, 'new_shortcode' => $select_store[0]->new_shortcode]);
        }
    }

    public function store(Request $request) {
        //$shop = session('shop');
        $shop = $_REQUEST['shop'];
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $setting_exist = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get()->count();

        if ($setting_exist > 0) {
            DB::table('general_settings')->where('storeid', $select_store[0]->id)->update(['number_of_blogs_per_row' => $request->number_of_blogs_per_row, 'number_of_products_per_row' => $request->number_of_products_per_row, 'blog_page_title' => '' . $request->blog_page_title . '', 'product_page_title' => '' . $request->product_page_title . '', 'dispaly_randon_blogs_if_not_assign' => ($request->dispaly_randon_blogs_if_not_assign == 1) ? "1" : "0", 'dispaly_randon_products_if_not_assign' => ($request->dispaly_randon_products_if_not_assign == 1) ? "1" : "0", 'with_slider' => ($request->with_slider == 1) ? "1" : "0", 'slider_type' => $request->slidertype, 'app_status' => $request->app_status, 'additional_css' => $request->additional_css]);
            //for updating the title for langify
            if($shop == "maillard.myshopify.com"){
                //dd($request->blog_page_title_fr);
                DB::table('langify_translate_title')->where('store_name', $shop)->update(['blog_page_title_fr' => $request->blog_page_title_fr, 'product_page_title_fr' => $request->product_page_title_fr]);
            }
        } else {
            DB::table('general_settings')->insert(['storeid' => $select_store[0]->id, 'number_of_blogs_per_row' => $request->number_of_blogs_per_row, 'number_of_products_per_row' => $request->number_of_products_per_row, 'blog_page_title' => '' . $request->blog_page_title . '', 'product_page_title' => '' . $request->product_page_title . '', 'dispaly_randon_blogs_if_not_assign' => ($request->dispaly_randon_blogs_if_not_assign == 1) ? "1" : "0", 'dispaly_randon_products_if_not_assign' => ($request->dispaly_randon_products_if_not_assign == 1) ? "1" : "0", 'with_slider' => ($request->with_slider == 1) ? "1" : "0", 'slider_type' => $request->slidertype, 'app_status' => $request->app_status, 'additional_css' => $request->additional_css]);
            //for saving the title for langify
            if($shop == "maillard.myshopify.com"){
                DB::table('langify_translate_title')->insert(['blog_page_title_fr' => $request->blog_page_title_fr, 'product_page_title_fr' => $request->product_page_title_fr, 'store_name' => $shop ]);
            }
        }
        $setting_exist = DB::table('general_settings')->where('storeid', $select_store[0]->id)->first();
        //$request->session()->flash('message', 'Your settings has been saved !');
        $notification = array(
            'message' => 'Your settings has been saved !',
            'alert-type' => 'success'
        );
        //return view('general_setting', ['settingArr' => $setting_exist, 'usersettings' => $select_store]);   
        
        return redirect()->route('generalsettings', ['shop' => $shop])->with('notification', $notification);

    }

    public function refresh_blog(Request $request) {
        $shop = $request['shop'];
        $app_settings = DB::table('appsettings')->where('id', 1)->first();        
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $blogArr = $sh->call(['URL' => '/admin/blogs.json', 'METHOD' => 'GET']);
        //dd($blogArr->blogs);
        if(count($blogArr->blogs) > 0){
            foreach($blogArr->blogs as $blog){
                $article = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                if($article->count > 0){
                    $article_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=250', 'METHOD' => 'GET']);

                    foreach($article_list->articles as $single_article){
                        //dd($single_article->handle);
                        $article_metafields = $sh->call(['URL' => '/admin/blogs/'.$blog->id.'/articles/'.$single_article->id.'/metafields.json?namespace=ly59047' , 'METHOD' => 'GET']);
                        //dd($article_metafields->metafields);
                        if(count($article_metafields->metafields) > 0){
                            foreach($article_metafields->metafields as $metafield_data){
                                //dd($metafield_data->key);
                                if($metafield_data->key == "title"){
                                    $related_blog_title = $metafield_data->value; 
                                }
                                if($metafield_data->key == "content"){
                                    $related_blog_description = strlen(strip_tags($metafield_data->value)) > 150 ? substr(strip_tags($metafield_data->value), 0, 50) . "..." : strip_tags($metafield_data->value); 
                                    $related_blog_description =  preg_replace('/[^A-Za-z0-9\-]/', '', $related_blog_description); 
                                }
                                
                            }
                            
                            $check_data = DB::table('article_langify_data')->where(['article_id' =>(string) $single_article->id,'blog_id' => (string)$blog->id])->get();
                            if(count($check_data) > 0){
                                $update_data = DB::table('article_langify_data')->where(['article_id' =>(string) $single_article->id,'blog_id' => (string)$blog->id])->update(['title' => $related_blog_title,'description' => $related_blog_description,'image_url' => $single_article->image->src,'handle' => $single_article->handle]);
                            }
                            else{
                                $insert_new_data = DB::table('article_langify_data')->insert(                   ['article_id' =>(string) $single_article->id,'blog_id' => (string)$blog->id,'title' => $related_blog_title,'description' => $related_blog_description,'image_url' => $single_article->image->src,'handle' => $single_article->handle,'language_id'=>'ly59047']);
                            }
                            
                            
                        }
                    }
                }
                
            }
            return 1;
            //dd($article_list->articles);
        }
        
        
    }
    public function refresh_product(Request $request) {
        $shop = $request['shop'];
        $app_settings = DB::table('appsettings')->where('id', 1)->first();        
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $updated_products = DB::table('new_product')->where('sync_status', 0)->get();
        if(count($updated_products) > 0){
            foreach($updated_products as $new_product){
                $productArr = $sh->call(['URL' => '/admin/products/'.$new_product->product_id.'.json', 'METHOD' => 'GET']);
                $product = $productArr->product;  
                $title = $product->title;
                $description = strlen(strip_tags($product->body_html)) > 50 ? substr(strip_tags($product->body_html), 0, 50) . "..." : strip_tags($product->body_html);
                
                    if(isset($product->image->src)){
                        $image_url = $product->image->src;
                    }
                    else{
                        $image_url = url('/image/default.png');
                    }
                
                $product_metafields = $sh->call(['URL' => '/admin/products/' . $product->id . '/metafields.json?namespace=ly59047', 'METHOD' => 'GET']);
                
                if(count($product_metafields->metafields) > 0){
                    foreach($product_metafields->metafields as $metafield_data){
                        if(isset($metafield_data->key)){
                            if($metafield_data->key == "title"){
                                $title = $metafield_data->value; 
                            }
                            if($metafield_data->key == "description"){
                                $description = strlen(strip_tags($metafield_data->value)) > 50 ? substr(strip_tags($metafield_data->value), 0, 50) . "..." : strip_tags($metafield_data->value); 
                                $description =  preg_replace('/[^A-Za-z0-9\-]/', '', $description); 
                                
                            }
                        }
                        
                    }
                    
                }
                $check_data = DB::table('product_langify_data')->where(['produci_id' =>(string) $product->id])->get();
                if(count($check_data) > 0){
                    
                    $update_data = DB::table('product_langify_data')->where(['produci_id' =>(string) $product->id])->update(['title' => $title,'description' => $description,'image_url' => $image_url,'handle' => $product->handle]);
                }
                else{
                    
                    $insert_new_data = DB::table('product_langify_data')->insert(['produci_id' =>(string) $product->id,'title' => $title,'description' => $description,'image_url' => $image_url,'handle' => $product->handle,'language_id'=>'ly59047']);

                }
                      
            }
            return 1;
        }

        
        
    }
    
}
