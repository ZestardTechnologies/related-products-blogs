<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;

class callbackController extends Controller {

    public function index(Request $request) {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            if (count($select_store) > 0) {
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('generalsettings', ['shop' => $shop ]);
                } else {
                    return redirect()->route('payment_process');
                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);

                $permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_products', 'write_products', 'read_content', 'write_content', 'read_themes', 'write_themes'), 'redirect' => $app_settings->redirect_url]);

                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name

            //Check if trial is still running
            $check_trial = DB::table('trial_info')->where('store_name', $shop)->first();
            if(count($check_trial) > 0){
                $total_trial_days = $check_trial->trial_days;
                $trial_activated_date = $check_trial->activated_on;
                $trial_over_date = $check_trial->trial_ends_on;
                $current_date = date("Y-m-d");
                //$current_date = "2018-12-12";
                
                if(strtotime($current_date) < strtotime($trial_over_date)){
                    $date1= date_create($trial_over_date);
                    $date2= date_create($current_date);
                    $trial_remain = date_diff($date2,$date1);
                    $new_trial_days = $trial_remain->format("%a");
                }
                else{
                    $new_trial_days = 0;
                }

                $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
                if (count($select_store) > 0) {
                    //Remove coment for the Payment method
                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                    $id = $select_store[0]->charge_id;
                    $url = 'admin/recurring_application_charges/' . $id . '.json';
                    $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                    $charge_id = $select_store[0]->charge_id;
                    $charge_status = $select_store[0]->status;
                    if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                        session(['shop' => $shop]);
                        return redirect()->route('generalsettings', ['shop' => $shop]);
                    } else {
                        return redirect()->route('payment_process');
                    }
                }

                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
                try {
                    $verify = $sh->verifyRequest($request->all());
                    if ($verify) {
                        $code = $request->input('code');
                        $accessToken = $sh->getAccessToken($code);
                        DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                        $shop_find = ShopModel::where('store_name', $shop)->first();
                        $shop_id = $shop_find->id;

                        $encrypt_id = crypt($shop_id, "ze");
                        $finaly_encrypt = str_replace(['/', '.'], "Z", $encrypt_id);

                        DB::table('usersettings')->where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                        //for creating the uninstall webhook
                        $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                        $webhookData = [
                            'webhook' => [
                                'topic' => 'app/uninstalled',
                                'address' => config('app.url') . 'uninstall.php',
                                'format' => 'json'
                            ]
                        ];
                        $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);

                        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
                        /* Below Code is done by Girish - Creating snippet file for product and blog list */
                        foreach ($theme->themes as $themeData) {
                            if ($themeData->role == 'main') {
                                $snippets_arguments = ['encrypt_id' => $finaly_encrypt];
                                $theme_id = $themeData->id;
                                
                                //api call for creating bloglist snippet
                                $view = (string) View('snippets_bloglist', $snippets_arguments);                            
                                
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/related-blogs-list.liquid', 'value' => $view]]]);

                                //api call for creating product snippet
                                $view = (string)View('snippets_productlist', $snippets_arguments);
                                
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/related-products-list.liquid', 'value' => $view]]]);
                            }
                        }
                        /* Creating snippet file for product and blog list */
                        
                        //api call for creating the app script tag
                        /* $script_url = "https://zestardshop.com/shopifyapp/related-products-and-blogs/public/js/related-products-and-blogs.js";
                        $script = $sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'POST', 'DATA' => ['script_tag' => ['event' => 'onload', 'src' => $script_url, 'display_scope' => 'all']]]); */
                        session(['shop' => $shop]);
                        if ($shop == "all-free-theme-test.myshopify.com") {//&& 2 == 2 //related-products-and-blogs.myshopify.com  //zankar-test.myshopify.com
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Related Products and Blogs',
                                        'price' => 0.01,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Terms & Condition Applied',
                                        'trial_days' => $new_trial_days,
                                        'test' => true
                                    )
                                )
                                    ], false);
                        } else {
                            //creating the Recuring charge for app
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Related-products-and-blogs',
                                        'price' => 7.99,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Terms & Condition Applied',
                                        'trial_days' => $new_trial_days,
                                     //'test' => true
                                    )
                                )
                                    ], false);
                        }

                        $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                        $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                        $msg = '<table>
                                <tr>
                                    <th>Shop Name</th>
                                    <td>' . $shopi_info->shop->name . '</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>' . $shopi_info->shop->email . '</td>
                                </tr>
                                <tr>
                                    <th>Domain</th>
                                    <td>' . $shopi_info->shop->domain . '</td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>' . $shopi_info->shop->phone . '</td>
                                </tr>
                                <tr>
                                    <th>Shop Owner</th>
                                    <td>' . $shopi_info->shop->shop_owner . '</td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td>' . $shopi_info->shop->country_name . '</td>
                                </tr>
                                <tr>
                                    <th>Plan</th>
                                    <td>' . $shopi_info->shop->plan_name . '</td>
                                </tr>
                            </table>';

                        $store_details = DB::table('development_stores')->where('dev_store_name', $shop)->first();

                        if (count($store_details) <= 0) {
                            mail("vijay.zestard@gmail.com", "Related-products-and-blogs App Installed", $msg, $headers);
                            mail("support@zestard.com", "Related-products-and-blogs App Installed", $msg, $headers);
                            mail("chandraprakash.zestard@gmail.com", "Related-products-and-blogs App Installed", $msg, $headers);
                        }

                        //redirecting to the Shopify payment page
                        echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                    } else {
                        // Issue with data
                    }
                } catch (Exception $e) {
                    echo '<pre>Error: ' . $e->getMessage() . '</pre>';
                }
            }
            else{
                //for the first time create trial
                $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
                if (count($select_store) > 0) {
                    //Remove coment for the Payment method
                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                    $id = $select_store[0]->charge_id;
                    $url = 'admin/recurring_application_charges/' . $id . '.json';
                    $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                    $charge_id = $select_store[0]->charge_id;
                    $charge_status = $select_store[0]->status;
                    if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                        session(['shop' => $shop]);
                        return redirect()->route('generalsettings', ['shop' => $shop]);
                    } else {
                        return redirect()->route('payment_process');
                    }
                }

                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
                try {
                    $verify = $sh->verifyRequest($request->all());
                    if ($verify) {
                        $code = $request->input('code');
                        $accessToken = $sh->getAccessToken($code);
                        DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                        $shop_find = ShopModel::where('store_name', $shop)->first();
                        $shop_id = $shop_find->id;

                        $encrypt_id = crypt($shop_id, "ze");
                        $finaly_encrypt = str_replace(['/', '.'], "Z", $encrypt_id);

                        DB::table('usersettings')->where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                        //for creating the uninstall webhook
                        $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                        $webhookData = [
                            'webhook' => [
                                'topic' => 'app/uninstalled',
                                'address' => config('app.url') . 'uninstall.php',
                                'format' => 'json'
                            ]
                        ];
                        $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);

                        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
                        /* Below Code is done by Girish - Creating snippet file for product and blog list */
                        foreach ($theme->themes as $themeData) {
                            if ($themeData->role == 'main') {
                                $snippets_arguments = ['encrypt_id' => $finaly_encrypt];
                                $theme_id = $themeData->id;
                                
                                //api call for creating bloglist snippet
                                $view = (string) View('snippets_bloglist', $snippets_arguments);                            
                                
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/related-blogs-list.liquid', 'value' => $view]]]);

                                //api call for creating product snippet
                                $view = (string)View('snippets_productlist', $snippets_arguments);
                                
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/related-products-list.liquid', 'value' => $view]]]);
                            }
                        }
                        /* Creating snippet file for product and blog list */
                        
                        //api call for creating the app script tag
                        /* $script_url = "https://zestardshop.com/shopifyapp/related-products-and-blogs/public/js/related-products-and-blogs.js";
                        $script = $sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'POST', 'DATA' => ['script_tag' => ['event' => 'onload', 'src' => $script_url, 'display_scope' => 'all']]]); */
                        session(['shop' => $shop]);
                        if ($shop == "all-free-theme-test.myshopify.com") {//&& 2 == 2 //related-products-and-blogs.myshopify.com  //zankar-test.myshopify.com
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Related Products and Blogs',
                                        'price' => 0.01,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Terms & Condition Applied',
                                        'trial_days' => 7,
                                        'test' => true
                                    )
                                )
                                    ], false);
                        } else {
                            //creating the Recuring charge for app
                            $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                            $charge = $sh->call([
                                'URL' => $url,
                                'METHOD' => 'POST',
                                'DATA' => array(
                                    'recurring_application_charge' => array(
                                        'name' => 'Related-products-and-blogs',
                                        'price' => 7.99,
                                        'return_url' => url('payment_success'),
                                        'capped_amount' => 20,
                                        'terms' => 'Terms & Condition Applied',
                                        'trial_days' => 7,
                                    //'test' => true
                                    )
                                )
                                    ], false);
                        }

                        $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                        $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                        $msg = '<table>
                                <tr>
                                    <th>Shop Name</th>
                                    <td>' . $shopi_info->shop->name . '</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>' . $shopi_info->shop->email . '</td>
                                </tr>
                                <tr>
                                    <th>Domain</th>
                                    <td>' . $shopi_info->shop->domain . '</td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>' . $shopi_info->shop->phone . '</td>
                                </tr>
                                <tr>
                                    <th>Shop Owner</th>
                                    <td>' . $shopi_info->shop->shop_owner . '</td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td>' . $shopi_info->shop->country_name . '</td>
                                </tr>
                                <tr>
                                    <th>Plan</th>
                                    <td>' . $shopi_info->shop->plan_name . '</td>
                                </tr>
                            </table>';

                        $store_details = DB::table('development_stores')->where('dev_store_name', $shop)->first();

                        if (count($store_details) <= 0) {
                            mail("vijay.zestard@gmail.com", "Related-products-and-blogs App Installed", $msg, $headers);
                            mail("support@zestard.com", "Related-products-and-blogs App Installed", $msg, $headers);
                            mail("chandraprakash.zestard@gmail.com", "Related-products-and-blogs App Installed", $msg, $headers);
                        }

                        //redirecting to the Shopify payment page
                        echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                    } else {
                        // Issue with data
                    }
                } catch (Exception $e) {
                    echo '<pre>Error: ' . $e->getMessage() . '</pre>';
                }

            }
            
        }
    }

    public function dashboard(Request $request){
		$shop = session('shop');
		if(empty($shop))
		{
			$shop = $_GET['shop'];
			//$shop = $request->input('shop');
			session(['shop' => $shop]);			
		}	
        //dd($shop);
        $id = DB::table('usersettings')->where('store_name', $shop)->value('id');

        return view('dashboard');
    }

    /* Payment Process */

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);

            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {

                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Related-products-and-blogs',
                                'price' => 7.99,
                                'return_url' => url('payment_success'),
                                'capped_amount' => 20,
                                'terms' => 'Terms & Condition Applied',
                               //'test' => true
                            )
                        )
                            ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {

                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('generalsettings', ['shop' => $shop ]);
                }
            }
        }
    }

    /* Payment Success */

    public function payment_compelete(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');

        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);
        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $trial_start = $Activatecharge_array['recurring_application_charge']->activated_on;
            $trial_end = $Activatecharge_array['recurring_application_charge']->trial_ends_on;
            $trial_days = $Activatecharge_array['recurring_application_charge']->trial_days;
            $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status,'activated_on' => $trial_start, 'trial_ends_on' => $trial_end]);

            //check if any trial info is exists or not
            if($trial_days > 0){
                $check_trial = DB::table('trial_info')->where('store_name', $shop)->first();
                if(count($check_trial) > 0){
                    DB::table('trial_info')->where('store_name', $shop)->update(['trial_days' => $trial_days,'activated_on' => $trial_start, 'trial_ends_on' => $trial_end ]);
                }
                else{
                    DB::table('trial_info')->insert([
                        'store_name' => $shop, 
                        'trial_days' => $trial_days,
                        'activated_on' => $trial_start,
                        'trial_ends_on' => $trial_end  
                    ]);
                }
            }

            return redirect()->route('generalsettings', ['shop' => $shop ]);
        } elseif ($status == "declined") {
            return redirect()->route('decline');
            //echo '<script>window.top.location.href="https://'.$shop.'/admin/apps"</script>';
        }
        //return redirect()->route('dashboard');
    }

    public function declined(Request $request) {
        $shop = session('shop');
        echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
    }
	public function update_modal_status(Request $request)
	{
		$shop = $request->input('shop_name');
		//For new installation modal status
		$shop_find = ShopModel::where('store_name' , $shop)->first();		
		DB::table('usersettings')->where('store_name' , $shop)->update(['new_install' => 'N']);				
	}

}
