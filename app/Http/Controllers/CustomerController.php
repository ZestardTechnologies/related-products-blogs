<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App;
use App\ShopModel;
class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $customer = $sh->call(['URL' => '/admin/customers/count.json','METHOD' => 'GET']);
        $customer_count = $customer->count;
       
        $total_customers = [];
        if($customer_count > 0){
            $customers = ceil($customer_count / 250);
            for ($i=0; $i<$customers; $i++) {
              $customer_list = $sh->call(['URL' => '/admin/customers.json','METHOD' => 'GET','DATA' => ['limit' => 250, 'page' => $i+1]]);
              $customer_list_array = $customer_list->customers;
              $total_customers = array_merge($total_customers, $customer_list_array);
            }
        }
        
        
        //dd($total_customers);
        return view('customer');
        //['customer_list' => $customer_list]
    }

    public function edit(Request $request,$id)
    {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $customer = $sh->call(['URL' => '/admin/customers/'.$id.'.json','METHOD' => 'GET']);
        return view('editcustomer',['customer' => $customer]);
    }
    
    public function get_customers(Request $request)
    {
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        /*$customer_list = $sh->call(['URL' => '/admin/customers.json','METHOD' => 'GET','DATA' => ['limit' => 250]]);
        //dd($customer_list->customers);
        $customers = array();
        $customers['data'] = array();
        foreach($customer_list->customers as $customer)
        {
            //array_push($customers,"customers",$customer->id);
            //array_push($customers,"customers",$customer->first_name);
            //array_push($customers,"customers",$customer->email);
            //array_push($customers,"customers",$customer->phone);
            
            $customers =  array('draw'=>"1",'recordsTotal'=>"57",'recordsFiltered'=>"57");         
            $customers['data'][] =  array($customer->id,$customer->first_name,$customer->email,$customer->phone);            
        }*/
        //print_r(json_encode($customers));
        //exit;
        $customer = $sh->call(['URL' => '/admin/customers/count.json','METHOD' => 'GET']);
        $customer_count = $customer->count;
       
        $total_customers = [];
        if($customer_count > 0){
            $customers = ceil($customer_count / 250);
            for ($i=0; $i<$customers; $i++) {
              $customer_list = $sh->call(['URL' => '/admin/customers.json','METHOD' => 'GET','DATA' => ['limit' => 250, 'page' => $i+1]]);
              $customer_list_array = $customer_list->customers;
              $total_customers = array_merge($total_customers, $customer_list_array);
            }
        }
        return json_encode($total_customers);
        //print_r(json_encode($customers));
        //['customer_list' => $customer_list]
    }
}
