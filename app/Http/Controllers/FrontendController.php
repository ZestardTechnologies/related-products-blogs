<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;
use App\ProductBlog;

class FrontendController extends Controller {

    public function Productindex(request $request) {
        
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $shop = $request->shop_name;                
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get();
        //dd($general_settings[0]);
        $number_of_products_per_row = $general_settings[0]->number_of_products_per_row;
        $slider_type = $general_settings[0]->slider_type;
        $app_status = $general_settings[0]->app_status;
        $additional_css = $general_settings[0]->additional_css;
        $limit = $general_settings[0]->number_of_blogs_per_row;
        $ProductBlog_modal = new ProductBlog;
        $product_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->get()->count();        
        $productData = '';

        $dispaly_randon_products_if_not_assign = $general_settings[0]->dispaly_randon_products_if_not_assign;

        
        if ($dispaly_randon_products_if_not_assign != '1') {            
            if ($product_count > 0) {

                $productArr = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->inRandomOrder()->get();                
                $val = 1;
                if ($slider_type == '1') {
                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                    foreach ($productArr as $product) {

                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);
                        $URLlink = "https://" . $shop . "/products/" . $product->handle;
                        $productTitle = strlen(strip_tags($productList->product->title)) > 25 ? substr(strip_tags($productList->product->title), 0, 25) . "..." : strip_tags($productList->product->title);

                        if($shop == 'littlerooms-jp.myshopify.com'){
                            $productData.= '<article class="thumbnail item">
                      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                              <div class="caption">
                                 <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                    <p itemprop="text" class="flex-text text-muted"></p></div>
                       </article>';
                        }else{
                            $productData.= '<article class="thumbnail item">
                            <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                    <div class="caption">
                                       <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                          <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                             </article>';
                        }
                        $val++;
                    }
                    $productData.= '</div>';
                } else if ($slider_type == '2') {
                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider">';
                    foreach ($productArr as $product) {
                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);
                        $URLlink = "https://" . $shop . "/products/" . $product->handle;
                        $productTitle = strlen(strip_tags($productList->product->title)) > 25 ? substr(strip_tags($productList->product->title), 0, 25) . "..." : strip_tags($productList->product->title);

                        if($shop == 'littlerooms-jp.myshopify.com'){
                            $productData.= '<article class="thumbnail item">
                      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                              <div class="caption">
                                 <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                    <p itemprop="text" class="flex-text text-muted"></p></div>
                       </article>';
                        }else{
                            $productData.= '<article class="thumbnail item">
                            <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                    <div class="caption">
                                       <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                          <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                             </article>';
                        }
                        $val++;
                    }
                    $productData.= '</div>';
                } else if ($slider_type == '3') {

                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="full-width-listId" class="full-width-list">';
                    foreach ($productArr as $product) {
                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;
                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);
                        $URLlink = "https://" . $shop . "/products/" . $product->handle;
                        $productTitle = strlen(strip_tags($productList->product->title)) > 25 ? substr(strip_tags($productList->product->title), 0, 25) . "..." : strip_tags($productList->product->title);
                        
                        if($shop == 'littlerooms-jp.myshopify.com'){
                            $productData.= '<article class="thumbnail item">
                      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                              <div class="caption">
                                 <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                    <p itemprop="text" class="flex-text text-muted"></p></div>
                       </article>';
                        }else{
                            $productData.= '<article class="thumbnail item">
                            <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                    <div class="caption">
                                       <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                          <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                             </article>';
                        }
                        $val++;
                    }
                    $productData.= '</div>';
                } else if ($slider_type == '4') {
                    $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                    $productData .= '<div id="sidebar-listId" class="sidebar-list">';
                    foreach ($productArr as $product) {
                        $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                        $default_image = url('/image/default.png');
                        $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                        $htmlContent = $productList->product->body_html;
                        $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                        $body_html2 = str_replace("<!--", "", $body_html1);
                        $body_html = str_replace("-->", "", $body_html2);
                        $URLlink = "https://" . $shop . "/products/" . $product->handle;
                        $productTitle = strlen(strip_tags($productList->product->title)) > 25 ? substr(strip_tags($productList->product->title), 0, 25) . "..." : strip_tags($productList->product->title);
                        
                        
                        if($shop == 'littlerooms-jp.myshopify.com'){
                            $productData.= '<article class="thumbnail item">
                      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                              <div class="caption">
                                 <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                    <p itemprop="text" class="flex-text text-muted"></p></div>
                       </article>';
                        }else{
                            $productData.= '<article class="thumbnail item">
                            <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                    <div class="caption">
                                       <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                          <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                             </article>';
                        }
                        $val++;
                    }
                    $productData.= '</div>';
                }
            }
        } else {
            
            $count = $sh->call(['URL' => '/admin/products/count.json?published_status=published', 'METHOD' => 'GET']);            
            $product_count = $count->count;
            //$limit = 5;
            //$current_page = 1;
            $limit = $number_of_products_per_row;
            if($shop == "broosa.myshopify.com")
            {
                //dd($product_count . " " . $limit);
            }
            $max_number = $product_count - $limit;
            $start = rand(0,$max_number);
            $current_page = ceil($start / $limit) + 1;
            
            $productArr = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page .'&published_status=published', 'METHOD' => 'GET']);            
            $val = 1;
            if ($slider_type == '1') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;
                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    
                    $productTitle = strlen(strip_tags($product->title)) > 25 ? substr(strip_tags($product->title), 0, 25) . "..." : strip_tags($product->title);
                    
                    if($shop == 'littlerooms-jp.myshopify.com'){
                        $productData.= '<article class="thumbnail item">
                  <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                          <div class="caption">
                             <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                <p itemprop="text" class="flex-text text-muted"></p></div>
                   </article>';
                    }else{
                        $productData.= '<article class="thumbnail item">
                        <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                      <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                         </article>';
                    }
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '2') {
                
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider">';                                
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;

                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    $productTitle = strlen(strip_tags($product->title)) > 25 ? substr(strip_tags($product->title), 0, 25) . "..." : strip_tags($product->title);                  
                   
               
                if($shop == 'littlerooms-jp.myshopify.com'){
                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
			<p itemprop="text" class="flex-text text-muted"></p></div>
               </article>';
                }else{
                    $productData.= '<article class="thumbnail item">
                    <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                            <div class="caption">
                               <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                     </article>';
                }
                    $val++;
                }
                $productData.= '</div>';    
                            
            } else if ($slider_type == '3') {

                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="full-width-listId" class="full-width-list">';
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;
                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    
                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    $productTitle = strlen(strip_tags($product->title)) > 25 ? substr(strip_tags($product->title), 0, 25) . "..." : strip_tags($product->title);
                    if($shop == 'littlerooms-jp.myshopify.com'){
                        $productData.= '<article class="thumbnail item">
                  <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                          <div class="caption">
                             <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                <p itemprop="text" class="flex-text text-muted"></p></div>
                   </article>';
                    }else{
                        $productData.= '<article class="thumbnail item">
                        <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                      <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                         </article>';
                    }
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '4') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="sidebar-listId" class="sidebar-list">';
                foreach ($productArr->products as $product) {

                    $default_image = url('/image/default.png');
                    $image_src = (isset($product->images[0])) ? $product->images[0]->src : $default_image;

                    $htmlContent = $product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $URLlink = "https://" . $shop . "/products/" . $product->handle;
                    $productTitle = strlen(strip_tags($product->title)) > 25 ? substr(strip_tags($product->title), 0, 25) . "..." : strip_tags($product->title);
                    if($shop == 'littlerooms-jp.myshopify.com'){
                        $productData.= '<article class="thumbnail item">
                  <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                          <div class="caption">
                             <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                <p itemprop="text" class="flex-text text-muted"></p></div>
                   </article>';
                    }else{
                        $productData.= '<article class="thumbnail item">
                        <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $productTitle . '</a></h4>
                      <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                         </article>';
                    }
                    $val++;
                }
                $productData.= '</div>';
            }
        }
        //Add additional css
        if (!empty($additional_css)) {
            $productData.= '<style>'.$additional_css.'</style>';
        }        
        $product_data = array($productData, $number_of_products_per_row, $slider_type, $app_status, $store_encrypt);
        
        return json_encode($product_data);
    }

    public function Blogindex(request $request) 
	{
        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $shop = $request->shop_name;
        
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get();
        
        $number_of_blogs_per_row = $general_settings[0]->number_of_blogs_per_row;
        $slider_type = $general_settings[0]->slider_type;
        $app_status = $general_settings[0]->app_status;
        $additional_css = $general_settings[0]->additional_css;
        $ProductBlog_modal = new ProductBlog;
		$blogArr = array();
        $blogData = '';

        $dispaly_randon_blogs_if_not_assign = $general_settings[0]->dispaly_randon_blogs_if_not_assign;
		$limit = $number_of_blogs_per_row;
		$current_page = 1;
        
		if($dispaly_randon_blogs_if_not_assign != '1') {			
            $blog_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $request->product_id . ""], ['status', '=', '1']])->get()->count();
            if($blog_count > 0) 
			{
				$blogArr = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $request->product_id . ""], ['status', '=', '1']])->inRandomOrder()->get();              
            }
			if($shop == "floridasfinesthoney.myshopify.com")
			{
				/* dd($blogArr->toArray()); */
			}
            $val = 1;			
            if($slider_type == '1') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                foreach ($blogArr as $blog) {                    
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);					
                    $default_image = url('/image/default.png');
                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $blogTitle = strlen(strip_tags($blog_data->article->title)) > 25 ? substr(strip_tags($blog_data->article->title), 0, 25) . "..." : strip_tags($blog_data->article->title);
                    //$link = 'edit_blog/' . $posts->id . '/articles/' . $posts->blog_id;
                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            } else if ($slider_type == '2') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider" style="width:26.02%;height:600px;margin:0 auto;overflow:hidden;display:block;">';
                foreach ($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);

                    $default_image = url('/image/default.png');

                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;
                    $blogTitle = strlen(strip_tags($blog_data->article->title)) > 25 ? substr(strip_tags($blog_data->article->title), 0, 25) . "..." : strip_tags($blog_data->article->title);

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            } else if ($slider_type == '3') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="full-width-listId" class="full-width-list">';
                
				foreach($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
			
                    $default_image = url('/image/default.png');

                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;
                    $blogTitle = strlen(strip_tags($blog_data->article->title)) > 25 ? substr(strip_tags($blog_data->article->title), 0, 25) . "..." : strip_tags($blog_data->article->title);
                    
                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            } else if ($slider_type == '4') {
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="sidebar-listId" class="sidebar-list" style="width:26.02%;margin:0 auto;overflow:hidden;display:block;">';
                foreach ($blogArr as $blog) {
                    $ArticleId = $blog->blogid;
                    $BlogId = $blog->post_blog_id;
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);

                    $default_image = url('/image/default.png');

                    $image_src = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;

                    $htmlContent = $blog_data->article->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);
                    $URLlink = "https://" . $shop . "/blogs/" . $blog->handle;
                    $blogTitle = strlen(strip_tags($blog_data->article->title)) > 25 ? substr(strip_tags($blog_data->article->title), 0, 25) . "..." : strip_tags($blog_data->article->title);

                    $blogData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $blogData.= '</div>';
            }

            //Changed by GIRISH
            //for blank the data if there is no blog selected
            $blog_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $request->product_id . ""], ['status', '=', '1']])->get()->count();
            if($blog_count <= 0){
                $blogData = "";
            }
        } 
		else 
		{			
            $val1 = 1;
			$current_page = 1;
            $limit = $number_of_blogs_per_row;
            //echo $slider_type;
            if($slider_type == '1') {
                $blogArr = $sh->call(['URL' => '/admin/blogs.json?status=any&limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                foreach ($blogArr->blogs as $blog) {
                    $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);
                    
                    //$limit = 5;                    
                    $blog_count = $blogPOST_countArr->count;
                    if ($blog_count > 0) {
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                        //$blogData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                        foreach ($blogPOST_list->articles as $articles) {
                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" . $blog->handle . "/" . $articles->handle;
                            
                            $blogTitle = strlen(strip_tags($articles->title)) > 25 ? substr(strip_tags($articles->title), 0, 25) . "..." : strip_tags($articles->title);
                            
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                    }
                }
                $blogData.= '</div>';
            } 
			else if ($slider_type == '2') 
			{       				
                $blogArr = $sh->call(['URL' => '/admin/blogs.json?status=any&limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider" style="width:26.02%;height:600px;margin:0 auto;overflow:hidden;display:block;">';
                foreach ($blogArr->blogs as $blog) {
                    $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);                    
                    //$limit = 5;                    
                    $blog_count = $blogPOST_countArr->count;
                    if ($blog_count > 0) {
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                        
                        foreach ($blogPOST_list->articles as $articles) {
                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" . $blog->handle . "/" . $articles->handle;
                            $blogTitle = strlen(strip_tags($articles->title)) > 25 ? substr(strip_tags($articles->title), 0, 25) . "..." : strip_tags($articles->title);
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                    }
                }
                $blogData.= '</div>';
                
            } 
			else if ($slider_type == '3') 
			{                				
                $blogArr = $sh->call(['URL' => '/admin/blogs.json?status=any&limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="full-width-listId" class="full-width-list">';                
				foreach ($blogArr->blogs as $blog) 
				{
                    $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);                    
                    //$limit = 5;                    
                    $blog_count = $blogPOST_countArr->count;
                    if($blog_count > 0) {                        
						$blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);                        
                        $all_articles = $blogPOST_list->articles;
						$articles = $all_articles[0];
						/* foreach($blogPOST_list->articles as $articles)  */
						{
                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" . $blog->handle . "/" . $articles->handle;
                            $blogTitle = strlen(strip_tags($articles->title)) > 25 ? substr(strip_tags($articles->title), 0, 25) . "..." : strip_tags($articles->title);
                            
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                    }
                }
                $blogData.= '</div>';                
            } 
			else if($slider_type == '4') 
			{                
                $blogArr = $sh->call(['URL' => '/admin/blogs.json?status=any&limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                $blogData = '<h3 class="blog_page_title">' . $general_settings[0]->blog_page_title . '</h3>';
                $blogData .= '<div id="sidebar-listId" class="sidebar-list" style="width:26.02%;margin:0 auto;overflow:hidden;display:block;">';
                foreach ($blogArr->blogs as $blog) {
                    $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);                    
                    //$limit = 5;                    
                    $blog_count = $blogPOST_countArr->count;
                    if ($blog_count > 0) {
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                        
                        foreach ($blogPOST_list->articles as $articles) {
                            $default_image = url('/image/default.png');
                            $image_src = (isset($articles->image)) ? $articles->image->src : $default_image;
                            $htmlContent = $articles->body_html;
                            $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                            $body_html2 = str_replace("<!--", "", $body_html1);
                            $body_html = str_replace("-->", "", $body_html2);
                            $URLlink = "https://" . $shop . "/blogs/" . $blog->handle . "/" . $articles->handle;
                            
                            $blogTitle = strlen(strip_tags($articles->title)) > 25 ? substr(strip_tags($articles->title), 0, 25) . "..." : strip_tags($articles->title);
                            
                            $blogData.= '<article class="thumbnail item">
                                <a class="blog-thumb-img" href="' . $URLlink . '" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                                <div class="caption">
                                   <h4 itemprop="headline"><a href="' . $URLlink . '" rel="bookmark">' . $blogTitle . '</a></h4>
                                  <p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
                               </article>';
                            $val1++;
                        }
                    }
                }
                $blogData.= '</div>';
                
            }
        }
        //Add additional css
        if (!empty($additional_css)) {
            $blogData.= '<style>'.$additional_css.'</style>';
        }
        
        
        $return_data = array($blogData, $number_of_blogs_per_row, $slider_type, $app_status, $store_encrypt);
        if($shop == "science-fun-kits.myshopify.com")
        {
            // echo "<pre>";
            // print_r($blogArr);            
            // dd($return_data);
        }
        return json_encode($return_data);
    }

    public function RandomProductindex(request $request) {

        $sh = App::make('ShopifyAPI');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop = session('shop');
        $shop = $request->shop_name;
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->get();



        $number_of_products_per_row = $general_settings[0]->number_of_products_per_row;
        $slider_type = $general_settings[0]->slider_type;
        $app_status = $general_settings[0]->app_status;
        $additional_css = $general_settings[0]->additional_css;
        $ProductBlog_modal = new ProductBlog;
        $product_count = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->get()->count();

        $productData = '';
        if ($product_count > 0) {
            $productArr = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $request->blog_id . ""], ['status', '=', '2']])->orderBy('id', 'DESC')->get();

            $val = 1;
            if ($slider_type == '1') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme full-width-slider">';
                foreach ($productArr as $product) {



                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');

                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;


                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '2') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="owl-example" class="owl-carousel owl-theme sidebar-slider">';
                foreach ($productArr as $product) {
                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');

                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '3') {

                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="full-width-listId" class="full-width-list">';
                foreach ($productArr as $product) {
                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');
                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;
                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            } else if ($slider_type == '4') {
                $productData = '<h3 class="product_page_title">' . $general_settings[0]->product_page_title . '</h3>';
                $productData .= '<div id="sidebar-listId" class="sidebar-list">';
                foreach ($productArr as $product) {
                    $productList = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    $default_image = url('/image/default.png');

                    $image_src = (isset($productList->product->image)) ? $productList->product->image->src : $default_image;

                    $htmlContent = $productList->product->body_html;
                    $body_html1 = strlen(strip_tags($htmlContent)) > 50 ? substr(strip_tags($htmlContent), 0, 50) . "..." : strip_tags($htmlContent);
                    $body_html2 = str_replace("<!--", "", $body_html1);
                    $body_html = str_replace("-->", "", $body_html2);

                    $productData.= '<article class="thumbnail item">
		      <a class="blog-thumb-img" href="#" title=""><img src="' . $image_src . '" class="img-responsive"></a>
                      <div class="caption">
                         <h4 itemprop="headline"><a href="#" rel="bookmark">' . $productList->product->title . '</a></h4>
			<p itemprop="text" class="flex-text text-muted">' . $body_html . '</p></div>
	  	     </article>';
                    $val++;
                }
                $productData.= '</div>';
            }
        }
        //Add additional css
        if (!empty($additional_css)) {
            $productData.= '<style>'.$additional_css.'</style>';
        }
        $product_data = array($productData, $number_of_products_per_row, $slider_type, $app_status, $store_encrypt);
        return json_encode($product_data);
    }

    //for the related product Langify
    public function get_related_product_list_langify_compatible(request $request){
        $shop = $request['shop_name'];
        $blog_id = $request['blog_id'];
        $language = $request['language'];
        $app_settings = DB::table('appsettings')->where('id', 1)->first();        
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->first();
        $langify_title = DB::table('langify_translate_title')->where('store_name', $shop)->first();
        $related_product_list = array();
        $response_data = array();
                
        //when the user has assaigned product
        if($general_settings->app_status == 1 and $general_settings->dispaly_randon_products_if_not_assign == 0 ){
            
            $product_list = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['blogid', '=', "" . $blog_id . ""], ['status', '=', '2']])->get();
            
            //dd($blog_id);
            if(count($product_list) > 0){
                foreach($product_list as $product){
                    
                    $product_data = $sh->call(['URL' => '/admin/products/' . $product->productid . '.json', 'METHOD' => 'GET']);
                    //dd($product_data);
                    $related_product_title = $product_data->product->title;
                    $related_product_description = strlen(strip_tags($product_data->product->body_html)) > 50 ? substr(strip_tags($product_data->product->body_html), 0, 50) . "..." : strip_tags($product_data->product->body_html);
                    //$product_data->product->body_html;
                    $related_product_image= $product_data->product->images[0]->src;
                    $related_product_handle = $product_data->product->handle;
                    $langify_product_data = DB::table('product_langify_data')->where(['produci_id' => $product->productid, 'language_id' => $language ])->first();
                    if(count($langify_product_data) > 0){
                        $related_product_title = $langify_product_data->title; 
                        $related_product_description = $langify_product_data->description; 
                    }
                    //api call for product metafields
                    // $product_metafields = $sh->call(['URL' => '/admin/products/' . $product->productid . '/metafields.json', 'METHOD' => 'GET']);
                    // foreach($product_metafields->metafields as $metafield_data){
                    //    //checking for the current language metafield data
                    //    if($metafield_data->namespace == $language){
                         
                    //      if($metafield_data->key == "title"){
                    //         $related_product_title = $metafield_data->value; 
                    //      }
                    //      if($metafield_data->key == "description"){
                    //         $related_product_description = $metafield_data->value; 
                    //      }

                    //    }   
                        
                    // }
                    //creating the response data for product
                    $product_data = [
                        'id' => $product_data->product->id,
                        'title' => $related_product_title,
                        'description' => $related_product_description,
                        'image' => $related_product_image,
                        'handle' => $related_product_handle
                    ];
                    
                    array_push($related_product_list,$product_data);
                }
                array_push($response_data,$general_settings);
                array_push($response_data,$related_product_list);
                array_push($response_data,$langify_title);
            } 
        }
        //when user has assign random product option
        elseif($general_settings->app_status == 1 and $general_settings->dispaly_randon_products_if_not_assign == 1){
            $count = $sh->call(['URL' => '/admin/products/count.json?published_status=published', 'METHOD' => 'GET']);
            $product_count = $count->count;
            $limit = $general_settings->number_of_products_per_row;
            $max_number = $product_count - $limit;
            $start = rand(0,$max_number);
            $current_page = ceil($start / $limit) + 1;
                        
            $productArr = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page .'&published_status=published', 'METHOD' => 'GET']);
            $product_list = $productArr->products;
            
            if(count($product_list) > 0){
                foreach($product_list as $product){
                    
                    //$product_metafields = $sh->call(['URL' => '/admin/products/' . $product->id . '/metafields.json', 'METHOD' => 'GET']);
                    $related_product_title = $product->title;
                    $related_product_description = strlen(strip_tags($product->body_html)) > 50 ? substr(strip_tags($product->body_html), 0, 50) . "..." : strip_tags($product->body_html);
                    //$product->body_html;
                    $related_product_image= $product->images[0]->src;
                    $related_product_handle = $product->handle;

                    //checking for the current language metafield data
                    // foreach($product_metafields->metafields as $metafield_data){
                       
                    //     if($metafield_data->namespace == $language){
                          
                    //       if($metafield_data->key == "title"){
                    //          $related_product_title = $metafield_data->value; 
                    //       }
                    //       if($metafield_data->key == "description"){
                    //          $related_product_description = $metafield_data->value; 
                    //       }
 
                    //     }   
                         
                    //  }
                     //creating the response data for product
                    if($language == "ly59047"){
                        //$langify_product_data = DB::table('product_langify_data')->where(['produci_id' => $product->productid, 'language_id' => $language ])->first();
                        $langify_product_data = DB::table('product_langify_data')->where(['language_id' => 'ly59047'])->get()->random($limit);
                        //dd($langify_product_data);
                        if(count($langify_product_data) > 0){
                            foreach($langify_product_data as $product_metafield_data){
                                $related_product_title = $product_metafield_data->title;
                                $related_product_description = $product_metafield_data->description;
                                // $product_data = [
                                //     'id' => $product->id,
                                //     'title' => $product_metafield_data->title,
                                //     'description' => $product_metafield_data->description,
                                //     'image' => $related_product_image,
                                //     'handle' => $related_product_handle
                                // ];        
                            }
                        }
                    }
                    // else{
                    //     $product_data = [
                    //         'id' => $product->id,
                    //         'title' => $related_product_title,
                    //         'description' => $related_product_description,
                    //         'image' => $related_product_image,
                    //         'handle' => $related_product_handle
                    //     ];
                    // }
                     $product_data = [
                         'id' => $product->id,
                         'title' => $related_product_title,
                         'description' => $related_product_description,
                         'image' => $related_product_image,
                         'handle' => $related_product_handle
                     ];
                     
                    array_push($related_product_list,$product_data);
                }
                array_push($response_data,$general_settings);
                array_push($response_data,$related_product_list);
                array_push($response_data,$langify_title);
            } 
        }
        else{
            $related_product_list = array();
            array_push($response_data,$general_settings);
            array_push($response_data,$langify_title);
        }
        
        return json_encode($response_data);
        
    }

    //for the related blog Langify 
    public function get_related_blog_list_langify_compatible(request $request){
        $shop = $request['shop_name'];
        $product_id = $request['product_id'];
        $language = $request['language'];
        $app_settings = DB::table('appsettings')->where('id', 1)->first();        
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
        $store_encrypt = $select_store[0]->store_encrypt;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $general_settings = DB::table('general_settings')->where('storeid', $select_store[0]->id)->first();
        $langify_title = DB::table('langify_translate_title')->where('store_name', $shop)->first();
        $related_blog_list = array();
        $response_data = array();

        //when the user has assaigned blog
        if($general_settings->app_status == 1 and $general_settings->dispaly_randon_blogs_if_not_assign == 0 ){
            $blog_list = ProductBlog::where([['storeid', '=', $select_store[0]->id], ['productid', '=', "" . $product_id . ""], ['status', '=', '1']])->get();
                        
            $related_blog_image = url('/image/default.png');
            
            if(count($blog_list) > 0){
                foreach($blog_list as $blog){
                                        
                    $blog_data = $sh->call(['URL' => '/admin/blogs/' . $blog->post_blog_id . '/articles/' . $blog->blogid . '.json', 'METHOD' => 'GET']);					
                    
                    $related_blog_title = $blog_data->article->title;
                    
                    $related_blog_description = strlen(strip_tags($blog_data->article->body_html)) > 50 ? substr(strip_tags($blog_data->article->body_html), 0, 50) . "..." : strip_tags($blog_data->article->body_html);
                    
                    $related_blog_image = (isset($blog_data->article->image)) ? $blog_data->article->image->src : $default_image;
                    
                    $related_blog_handle = $blog->handle;
                    
                    $langify_blog_data = DB::table('article_langify_data')->where(['article_id' => $blog->blogid, 'blog_id' => $blog->post_blog_id, 'language_id' => $language ])->first();
                    if(count($langify_blog_data) > 0){
                        $related_blog_title = $langify_blog_data->title;
                        $related_blog_description = $langify_blog_data->description;
                    }
                    //api call for article metafields  
                    //$blog_metafields = $sh->call(['URL' => '/admin/blogs/'.$blog->post_blog_id.'/articles/'.$blog->blogid.'/metafields.json' , 'METHOD' => 'GET']);
                    //dd($blog_metafields->metafields);

                    //checking for the current language metafield data
                    // foreach($blog_metafields->metafields as $metafield_data){
                        
                    //    if($metafield_data->namespace == $language){
                                                  
                    //      if($metafield_data->key == "title"){
                    //         $related_blog_title = $metafield_data->value; 
                    //      }
                    //      if($metafield_data->key == "content"){
                    //         $related_blog_description = strlen(strip_tags($metafield_data->value)) > 50 ? substr(strip_tags($metafield_data->value), 0, 50) . "..." : strip_tags($metafield_data->value);                                                        
                    //      }

                    //    }   
                        
                    // }
                    // dd($blog->blogid);
                    $blog_data = [
                        'id' => $blog->blogid,
                        'title' => $related_blog_title,
                        'description' => $related_blog_description,
                        'image' => $related_blog_image,
                        'handle' => $related_blog_handle
                    ];
                     
                    array_push($related_blog_list,$blog_data);
                }
                
                array_push($response_data,$general_settings);
                array_push($response_data,$related_blog_list);
                array_push($response_data,$langify_title);
                
            } 
        }
        //when user has assign random blog option
        elseif($general_settings->app_status == 1 and $general_settings->dispaly_randon_blogs_if_not_assign == 1){
            $limit = $general_settings->number_of_blogs_per_row;
            
            $blogArr = $sh->call(['URL' => '/admin/blogs.json?limit='.$limit, 'METHOD' => 'GET']);
            if(count($blogArr)){
                foreach ($blogArr->blogs as $blog) {
                    $blogPOST_countArr = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles/count.json', 'METHOD' => 'GET']);                    
                    $blog_count = $blogPOST_countArr->count;
                    
                    if ($blog_count > 0) {
                        //$blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json', 'METHOD' => 'GET']);
                        $blogPOST_list = $sh->call(['URL' => '/admin/blogs/' . $blog->id . '/articles.json?limit='.$limit, 'METHOD' => 'GET']);
                        foreach ($blogPOST_list->articles as $articles) {                          
                            $related_blog_title = $articles->title;
                            $related_blog_description = strlen(strip_tags($articles->body_html)) > 150 ? substr(strip_tags($articles->body_html), 0, 50) . "..." : strip_tags($articles->body_html);
                            $related_blog_image = $articles->image->src;
                            $related_blog_handle = $blog->handle.'/'.$articles->handle;
                            
                            // $blog_metafields = $sh->call(['URL' => '/admin/blogs/'.$blog->id.'/articles/'.$articles->id.'/metafields.json' , 'METHOD' => 'GET']);
                            
                            // foreach($blog_metafields->metafields as $metafield_data){
                       
                            //     if($metafield_data->namespace == $language){
                                  
                            //       if($metafield_data->key == "title"){
                            //          $related_blog_title = $metafield_data->value; 
                            //       }
                            //       if($metafield_data->key == "content"){
                            //          $related_blog_description = strlen(strip_tags($metafield_data->value)) > 250 ? substr(strip_tags($metafield_data->value), 0, 50) . "..." : strip_tags($metafield_data->value); 
                            //          //$related_blog_description =  preg_replace('/[^A-Za-z0-9\-]/', '', $related_blog_description); 
                            //       }
         
                            //     }      
                            //  }
                            // if($language == "ly59047"){
                            //     $langify_blog_data = DB::table('article_langify_data')->where(['language_id' => $language ])->get()->random($limit);
                            //     //dd($langify_blog_data->item->id);
                            //     if(count($langify_blog_data) > 0){
                            //         foreach($langify_blog_data as $random_blog){
                                        
                            //             $blog_data = [
                            //                 'id' => $random_blog->id,
                            //                 'title' => $random_blog->title,
                            //                 'description' => $random_blog->description,
                            //                 'image' => $random_blog->image_url,
                            //                 'handle' => $random_blog->handle
                            //             ];
                            //             array_push($related_blog_list,$blog_data);
                            //         }
                                
                                 
                            //     }
                                
                            // }
                            // else{
                            //     $blog_data = [
                            //         'id' => $blog->id,
                            //         'title' => $related_blog_title,
                            //         'description' => $related_blog_description,
                            //         'image' => $related_blog_image,
                            //         'handle' => $related_blog_handle
                            //     ];
                            //     array_push($related_blog_list,$blog_data);
                            // }
                             
                        }
                    }
                    if($language == "ly59047"){
                        $langify_blog_data = DB::table('article_langify_data')->where(['language_id' => $language ])->get()->random($limit);
                        //dd($langify_blog_data->item->id);
                        if(count($langify_blog_data) > 0){
                            foreach($langify_blog_data as $random_blog){
                                
                                $blog_data = [
                                    'id' => $random_blog->id,
                                    'title' => $random_blog->title,
                                    'description' => $random_blog->description,
                                    'image' => $random_blog->image_url,
                                    'handle' => "articles/".$random_blog->handle
                                ];
                                array_push($related_blog_list,$blog_data);
                            }
                        
                         
                        }
                        
                    }
                    else{
                        $blog_data = [
                            'id' => $blog->id,
                            'title' => $related_blog_title,
                            'description' => $related_blog_description,
                            'image' => $related_blog_image,
                            'handle' => $related_blog_handle
                        ];
                        array_push($related_blog_list,$blog_data);
                    }
                }
                array_push($response_data,$general_settings);
                array_push($response_data,$related_blog_list);
                array_push($response_data,$langify_title);
            }
        }
        else{
            $related_product_list = array();
            array_push($response_data,$general_settings);
            array_push($response_data,$langify_title);
        }  
        return json_encode($response_data);
    }

}
