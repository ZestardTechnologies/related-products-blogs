<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBlog extends Model
{
    protected $table = 'blog_product';
}
