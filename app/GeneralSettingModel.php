<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralSettingModel extends Model
{
    protected $table = 'general_setting';
}
