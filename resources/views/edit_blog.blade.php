@extends('header')
@section('content')
<div class="container">
    <div class="row">
        <div class="card">
            <div class="overlay"></div>
            <div class="basic-container dashboardpage">
                <div class="">
                    <div class="success-copied"></div>
                    <div class="col-sm-12">
                        <div class="bd-example">
                            <!--<form method="post" action="{{ url('blog/update/'.$blog_data->article->id) }}" onsubmit="startloader(1)"> -->
                            <!--<form method="POST" id="form_productSave"> -->                                
                            <div class="form-group">

                                <label class="control-label " for="title">Title</label> {{ $blog_data->article->title }}
                                <!--<input class="form-control rpb_blog_title" name="title" type="text" value="<?php //echo $blog_data->article->title;  ?>" readonly="" />-->
                                <input type="hidden" class="form-control" name="blogid" id="blogid" value="{{ $blog_data->article->id }}"> 
                                <input type="hidden" class="form-control" name="post_blog_id" id="post_blog_id" value="{{ $blog_data->article->blog_id }}"> 
                                <a href="{{ url('bloglist') }}?shop=<?php echo $_REQUEST['shop']; ?>"> <button class="btn btn-default back_button" type="button"> Back </button> </a>
                            </div>
                            <?php $default_image = url('/image/default.png'); ?>
                            <div class="form-group">            
                                <label for="producttitle">Blog Image</label> <br> 
                                <img src="@if(isset($blog_data->article->image)) {{ $blog_data->article->image->src }} @else {{ $default_image }} @endif" width="80px" style="padding: 5px;border: 1px solid #f8f8f8;" />

                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered" id="productlisttbl" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="width:10px !important;">#</th>
                                                <th style="width:10px !important;">Select/Unselect Products</th>
                                                <th>Product Image</th>
                                                <th>Product Name</th>
                                            </tr>
                                        </thead>                                            
                                    </table>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="btnSubmit" name="content" type="submit">Submit</button>
                            </div>
                            <!--</form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function startloader(process) {
        if (process == 1) {
            $(".overlay").css({
                'display': 'block',
                'background-image': 'url({{ asset("image/loader.gif") }})',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background-position': 'center'
            });
        } else {
            $(".overlay").css({
                'display': 'none',
                'background-image': 'none',
            });
        }
    }

    $(document).ready(function () {

        var product_ids_data = <?php echo ($product_blogDB != '') ? $product_blogDB : '' ?>;

        var selected_products = [];
        if (product_ids_data) {
            var selected_products_array = Object.values(<?php echo $product_blogDB; ?>);
            selected_products = selected_products_array;
        }
        //console.log(selected_products);
        
        var blogid = $('#blogid').val();
        var post_blog_id = $('#post_blog_id').val();
        var table = $('#productlisttbl').DataTable({
            "pageLength": 10,
            "lengthChange": false,
            "paging": true,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "{{url('get_productlist_for_editblogs')}}?shop=<?php echo $_REQUEST['shop']; ?>",
                "type": "POST",
                "data": function (d) {
                    d.blogid = blogid;
                    d.post_blog_id = post_blog_id;
                }
            },
            "cache": false,
            "columns": [
                {
                    "data": "0", "orderable": false
                },
                {
                    "data": "1", "orderable": false
                },
                {
                    "render": function (data, type, JsonResultRow, meta) {
                        return '<img src="' + data + '" style="width:50px;">';
                    }
                },
                {
                    "data": "3", "orderable": false
                },
            ],
            "order": [[1, 'asc']]
        });

        $(document).ajaxComplete(function () {
            $(".select-productcheckbox").click(function () {
                if (jQuery.inArray($(this).val(), selected_products) !== -1) {
                    //console.log("ifclick");
                    var uncheck_productId = selected_products.indexOf($(this).val());
                    selected_products.splice(uncheck_productId, 1);
                    //console.log(selected_products);
                } else {
                    //console.log("elseclick");
                    selected_products.push($(this).val());
                    //console.log(selected_products);
                }
            });
            if (selected_products) {
                $(".select-productcheckbox").each(function () {
                    var checkIds = jQuery.inArray($(this).val(), selected_products);
                    if (checkIds !== -1) {
                        $(this).attr('checked', true);
                    } else {
                        $(this).attr('checked', false);
                    }
                });
            }
        });

        $("#btnSubmit").on('click', function (event) {
            startloader(1);
            saveproduct_ids(event);
        });

        function saveproduct_ids(event) {

            var blogid = $('#blogid').val();
            var post_blog_id = $('#post_blog_id').val();

            $.ajax({
                type: "POST",
                url: "{{ url('blog/update/'.$blog_data->article->id) }}?shop=<?php echo $_REQUEST['shop']; ?>",
                data: {
                    data: selected_products,
                    blogid: blogid,
                    post_blog_id: post_blog_id,
                    _token: "{{ csrf_token() }}",
                },
                success: function (msg) {
                    toastr.options = {
                        "closeButton": true,
                    }
                    toastr.success("Your products are asigned to these blog!");
                    var href = "{{ url('bloglist') }}?shop=<? echo $_REQUEST['shop']; ?>";
                    window.location.replace(href);
                    //location.reload();
                },
                error: function (jqXHR, exception) {
                    toastr.options = {
                        "closeButton": true,
                    }
                    toastr.error("Could not complete your request, Please try again later.");
                }
            });
        }
    });


</script>
@endsection

