@extends('header')
@section('content')

<div class="basic-container dashboardpage">    
    <div class="">          
        <div class="success-copied"></div>           
        <div class="col-sm-6">      
            <div class="bd-example">
                <form method="post" action="{{ url('blog/store') }}">
                    {{ csrf_field() }}
                    <div class="form-group"> <!-- Name field -->
                        <label class="control-label " for="title">Import</label>
                        <input class="form-control" name="title" type="file"/>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary " name="content" type="submit">Submit</button>
                    </div>

                </form>								
            </div>
        </div>    
    </div>
</div>
@endsection