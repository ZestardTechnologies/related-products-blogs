@extends('header')

@section('content')

    <div class="basic-container dashboardpage">

        <div class="">  

            <div class="success-copied"></div>   	

                <div class="col-sm-12">

                <table id="customer_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px !important;">Customer Id</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Customer Phone</th>
                         
                        </tr>
                    </thead>
                    
                </table>
        
                </div>

        </div>

    </div>
    <script>
      $(document).ready(function() {
          $('#customer_list').DataTable({
            "paging":   true,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax": '{{url('getcustomers')}}'
          });
        });
    </script>
@endsection