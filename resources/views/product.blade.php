@extends('header')
@section('content')

<div class="container">
    <div class="row">
        <div class="card">
            <div class="basic-container dashboardpage">
                <div class="">          
                    <div class="success-copied"></div>                     
                    <div class="col-sm-12">            
                        <h2 class="sub-top-heading">Products List</h2>
                        <table id="product_list" class="table table-striped table-bordered" cellspacing="0" width="100%">                
                            <thead>                    
                                <tr>                        
                                    <th style="display:none">#</th>
                                    <th style="width:10px !important;">#</th> 
                                    <th>Product Images</th>
                                    <th>Product Name</th> 
                                    <th>Assigned Blog Count</th>
                                    <th style="width:65px !important;" class="action-shorting">Action</th>                    
                                </tr>                
                            </thead>   
                        </table>
                    </div>    
                </div>
            </div>
        </div>    
    </div>
</div>

<script>
    //var parent_url = document.frames['0'].document.location;
    //alert(parent_url);
    //alert("anuj");
    //var shop_name = parent_url.substring(6);
    var shop_name = '<?php echo $shop; ?>';
    jQuery(document).ready(function () {
        jQuery('#product_list').DataTable({
            "pageLength": 10,            
            "lengthChange": false,
            "paging": true,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "ajax": {"url": '{{url('getproducts')}}',"data": 
                        {"shop":shop_name}},
            "cache": false,
            "columns": [
                {
                    "data": "0", "orderable": false
                },
                {
                    "render": function (data, type, JsonResultRow, meta) {                        
                        return '<img src="' + data + '" style="width:50px;">';
                    }
                },
                {
                    "data": "2", "orderable": false
                },
                {
                    "data": "3", "orderable": false
                },
                {
                    "render": function (data, type, JsonResultRow, meta) {                        
                        return '<a href="edit_product/' + data + '?shop=<?php echo $_REQUEST['shop']; ?>"><span class="glyphicon glyphicon-edit"></span></a>';
                    }
                }],
            "order": [[1, 'asc']]
        });

    });
</script>

@endsection