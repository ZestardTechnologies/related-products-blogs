@extends('header')
@section('content')
<?php
$store_name = session('shop');
?>
<div class="overlay"></div>
<div class="container">
    <div class="row">
        <div class="card">
            <div class="basic-container dashboardpage">
                <div class="generaldiv">
                    <div class="success-copied"></div>
                    <div class="col-sm-12 col-md-12">
                    <h2 class="sub-top-heading">General Setting</h2>
                    <form method="POST" action="{{URL('setting/store')}}" onsubmit="startloader(1)">
                            <!--@if(session()->has('message'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                {!! session('message') !!}
                            </div>
                            @endif-->

                            {{-- {{ csrf_field() }}  --}}
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="col-md-12 form-group">
                                <div class="col-md-6">
                                    <label for="app_status">Enable App?</label>
                                    <select class="form-control rpb-block" id="app_status" name="app_status">
                                        <option value="1" <?php if ($settingArr) { echo ($settingArr->app_status == 1) ? 'selected="selected"' : ''; } ?>>Enabled</option>
                                        <option value="0" <?php if ($settingArr) { echo ($settingArr->app_status == 0) ? 'selected="selected"' : ''; } ?>>Disabled</option>
                                    </select>
                                </div>

                                <div class="slidertypedivcls col-md-6">                                
                                    <label for="slidertype">Select Slider Type(select one):</label>
                                    <select name="slidertype" class="form-control rpb-block" id="slidertype">
                                        <option value="1" <?php if ($settingArr) { echo ($settingArr->slider_type == 1) ? 'selected="selected"' : ''; } ?>>Full width slider</option>
                                        <option value="2" <?php if ($settingArr) { echo ($settingArr->slider_type == 2) ? 'selected="selected"' : ''; } ?>>Sidebar slider</option>
                                        <option value="3" <?php if ($settingArr) { echo ($settingArr->slider_type == 3) ? 'selected="selected"' : ''; } ?>>Full width list</option>
                                        <option value="4" <?php if ($settingArr) { echo ($settingArr->slider_type == 4) ? 'selected="selected"' : ''; } ?>>Sidebar list</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="right related-blogs col-md-6">
                                    <label for="blog_page_title">Blog page title ( for product list )</label>
                                    <input type="text" name="blog_page_title" class="form-control" value="<?php if ($settingArr) {
                                            echo isset($settingArr) ? $settingArr->blog_page_title : '';
                                        } ?>">
                                    <!--for maillard langify title start -->
                                    @if($store_name == "maillard.myshopify.com")
                                    <label for="blog_page_title_fr">Blog page title for Français ( for product list )</label>
                                    <input type="text" name="blog_page_title_fr" class="form-control" value="<?php if ($langify_settings) {
                                        echo isset($langify_settings) ? $langify_settings->blog_page_title_fr : '';
                                    } ?>">
                                    @endif
                                    <!--for maillard langify title end -->
                                    <p class="related_pb_note"><b>Note: </b>It will show Blog Slider Title in Product page at front end.</p>
                                </div>

                                <div class="right related-blogs col-md-6">
                                    <label for="product_page_title">Product page title ( for blog list )</label>
                                    <input type="text" name="product_page_title" class="form-control" value="<?php if ($settingArr) {
                                            echo isset($settingArr) ? $settingArr->product_page_title : '';
                                        } ?>">
                                    <!--for maillard langify title start -->
                                    @if($store_name == "maillard.myshopify.com")
                                    <label for="product_page_title_fr">Product page title for Français ( for blog list )</label>
                                    <input type="text" name="product_page_title_fr" class="form-control" value="<?php if ($langify_settings) {
                                        echo isset($langify_settings) ? $langify_settings->product_page_title_fr : '';
                                    } ?>">
                                    @endif
                                    <!--for maillard langify title end -->
                                    <p class="related_pb_note"><b>Note: </b>It will show Product Slider Title in Blog page at front end.</p>
                                </div>
                            </div>

                            <div class="right related-products related-pb-div col-md-12">
                                <div class="col-md-12">
                                    <label for="blog-number">Number of blogs per row</label>
                                    <input type="text" name="number_of_blogs_per_row" class="form-control number_per_row" value="<?php if ($settingArr) {
                                        echo isset($settingArr) ? $settingArr->number_of_blogs_per_row : '';
                                    } ?>">
                                    <?php 
                                    if($store_name == "maillard.myshopify.com"){
                                    ?>
                                        <button id="blog_refresh" name="blog_refresh" type="button" onclick="RefreshBlogData()" class="btn tooltipped tooltipped-s " style="display: block;float:right;">Sync Blog</button>
                                            
                                    <?php
                                        }
                                    ?>
                                    <p class="related_pb_note"><b>Note: </b>It will apply only when you select <b>Full width slider</b>.</p>
                                </div>
                                <div class="col-md-12 show_checkbox">                        
                                    <input type="checkbox" name="dispaly_randon_blogs_if_not_assign" value="1" <?php
                                        if ($settingArr) {
                                            if ($settingArr->dispaly_randon_blogs_if_not_assign == '1') {
                                                echo "checked";
                                            }
                                        }
                                        ?>> Display random blogs if not assign
                                </div>
                                <div class="col-md-12">
                                    <label for="maximum-products">Shortcode for Blog Slider in product page</label>
                                    <div class="view-shortcode" style="display: -webkit-box;">
                                        @if($new_shortcode == 'Y')
											<textarea id="product-shortcode" rows="1" class="form-control short-code" readonly=""><?php echo "{% include 'related-blogs-list' %}"; ?></textarea>											
										@else	
											<textarea id="product-shortcode" rows="1" class="form-control short-code" readonly=""><?php echo "<div class='zestard-bloglist' store_id='" . $usersettings[0]->store_encrypt . "' product_id='{{ product.id }}'></div>"; ?></textarea>
                                        @endif
                                        <button id="copyproductBtn" type="button" onclick="copyToClipboard('#product-shortcode')" class="btn tooltipped tooltipped-s copyMe" style="display: block;"> <i class="fa fa-check"></i>Copy to clipboard</button>
                                    </div>                                    
                                </div>
                                <div class="col-md-12">
                                    <p class="related_pb_note"><b>Note: </b>If you want to show blog slider in product page then copy and paste the above shortcode in <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a></p>
                                </div>                                
                            </div>

                            <div class="right related-blogs related-pb-div col-md-12">
                                <div class="col-md-12">
                                    <label for="products-number">Number of products per row</label>
                                    <input type="text" name="number_of_products_per_row" class="form-control number_per_row" value="<?php if ($settingArr) {
                                        echo isset($settingArr) ? $settingArr->number_of_products_per_row : '';
                                    } ?>">
                                    <?php 
                                    if($store_name == "maillard.myshopify.com"){
                                    ?>
                                        <button id="product_refresh" name="product_refresh" type="button" onclick="RefreshProductData()" class="btn tooltipped tooltipped-s " style="display: block;float:right;">Sync Product</button>
                                            
                                    <?php
                                        }
                                    ?>
                                    <p class="related_pb_note"><b>Note: </b>It will apply only when you select <b>Full width slider</b>.</p>
                                </div>
                                <div class="col-md-12 show_checkbox">
                                    <input type="checkbox" name="dispaly_randon_products_if_not_assign" value="1" <?php
                                    if ($settingArr) {
                                        if ($settingArr->dispaly_randon_products_if_not_assign == '1') {
                                            echo "checked";
                                        }
                                    }
                                    ?>> Display random products if not assign
                                </div>
                                <div class="col-md-12">
                                    <label for="maximum-blogs">Shortcode for Product Slider in blog page</label>
                                    <div class="view-shortcode" style="display: -webkit-box;">
                                        @if($new_shortcode == 'Y')
											<textarea id="blog-shortcode" rows="1" class="form-control short-code" readonly=""><?php echo "{% include 'related-products-list' %}"; ?></textarea>										
										@else
											<textarea id="blog-shortcode" rows="1" class="form-control short-code" readonly=""><?php echo "<div class='zestard-productlist' store_id='" . $usersettings[0]->store_encrypt . "' blog_id='{{ article.id }}'></div>"; ?></textarea>										
										@endif
                                        <button id="copyblogBtn" type="button" onclick="copyToClipboard('#blog-shortcode')" class="btn tooltipped tooltipped-s copyMe" style="display: block;"> <i class="fa fa-check"></i>Copy to clipboard</button>
                                    </div>                                    
                                </div>
                                <div class="col-md-12">
                                    <p class="related_pb_note"><b>Note: </b>If you want to show product slider in blog page then copy and paste the above shortcode in <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid</b></a></p>
                                </div>
                            </div>
                            <div class="right related-products related-pb-div col-md-12 form-group">
                                <div class="right related-blogs col-md-12">
                                    <label for="additional_css">Additional CSS</label>
                                    <textarea name="additional_css" id="additional_css" class="form-control" rows="4">@if($settingArr)@if($settingArr->additional_css) {{ $settingArr->additional_css }} @endif @endif</textarea>
                                    <p class="related_pb_note"><b>Note: </b>You can add CSS For Example: .body {margin:0px;}.</p>
                                </div>
                            </div>
                            <input type="hidden" name="shop" value="<?php echo $_REQUEST['shop']; ?>" />
<!--                            <div class="right related-blogs related-pb-div">
                                <div class="col-md-12">
                                    <label for="show-random">Show random product on blog listing page</label>
                                    <div class="view-shortcode" style="display: -webkit-box;">
                                        <textarea id="randomproduct-shortcode" rows="1" class="form-control short-code" readonly=""><?php //echo "<div class='zestard-randomproductlist' store_id='" . $usersettings[0]->store_encrypt . "' blog_id='{{ article.id }}'></div>"; ?></textarea>
                                        <button id="copyrandomproductBtn" type="button" onclick="copyToClipboard('#randomproduct-shortcode')" class="btn tooltipped tooltipped-s copyMe" style="display: block;"> <i class="fa fa-check"></i>Copy to clipboard</button>
                                    </div>                                    
                                </div>
                                <div class="col-md-12">
                                    <p class="related_pb_note"><b>Note: </b>If you want to show random product slider in blog page then copy and paste the above shortcode in <a href="https://<?php //echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid.</b></a> If you already pasted the product page shortcode then you can not paste this shortcode. Choose one from product shortcode or random product shortcode.</p>
                                </div>
                            </div>-->
                                    
                            <button type="submit" class="btn btn-primary onclick_submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="new_note">
    <div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
				<p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong>Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#dont_show_again").change(function(){
		var checked   = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";			
		if(!checked)
		{
			$.ajax({
				url:'update-modal-status',
				data:{shop_name:shop_name},
				async:false,					
				success:function(result)
				{
					
				}
			});				
			$('#new_note').modal('toggle');
		}
	});
	if("{{ $new_install }}")
	{		
		var new_install = "{{ $new_install }}";	
		if(new_install == "Y")
		{
			$('#new_note').modal('show');
		}
	}	
});
</script>	
@endsection
<script>
    
    var el1 = document.getElementById('copyproductBtn');
    var el2 = document.getElementById('copyblogBtn');
    var el3 = document.getElementById('copyrandomproductBtn');
    if (el1) {
        el1.addEventListener("click", function () {
            copyToClipboard(document.getElementById("product-shortcode"));
        });
    }
    if (el2) {
        el2.addEventListener("click", function () {
            copyToClipboard(document.getElementById("blog-shortcode"));
        });
    }
    if (el3) {
        el3.addEventListener("click", function () {
            copyToClipboard(document.getElementById("randomproduct-shortcode"));
        });
    }
    /*document.getElementById("copyproductBtn").addEventListener("click", function () {
        copyToClipboard(document.getElementById("product-shortcode"));
    });
    document.getElementById("copyblogBtn").addEventListener("click", function () {
        copyToClipboard(document.getElementById("blog-shortcode"));
    });
    document.getElementById("copyrandomproductBtn").addEventListener("click", function () {
        copyToClipboard(document.getElementById("randomproduct-shortcode"));
    });*/


    function copyToClipboard(elem) {
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        if (isInput) {
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            target.textContent = "";
        }
        return succeed;
    }
</script>
<script type="text/javascript">
  function startloader(process) {
      if(process == 1){
        $(".overlay").css({  
            'display' : 'block',
            'background-image' : 'url({{ asset("image/loader.gif") }})',
            'background-repeat': 'no-repeat',
            'background-attachment': 'fixed',
            'background-position': 'center'
        });  
      } else{
        $(".overlay").css({   
            'display' : 'none',
            'background-image' : 'none',
        });  
      }    
  }
  function RefreshBlogData(){
    //startloader(1);
    $.ajax({
        async: false,
        type: "GET",
        url: '{{ url('BlogRefresh') }}',
        data: {
            'shop': '{{ $store_name }}'       
        },
        success: function(data) {
            console.log(data);
            alert('Blog Data has Sync successfully');
            //startloader(0);
        } 
    });
  }
  function RefreshProductData(){
    $.ajax({
        async: false,
        type: "GET",
        url: '{{ url('ProductRefresh') }}',
        data: {
            'shop': '{{ $store_name }}'       
        },
        success: function(data) {
            console.log(data);
            alert('Product Data has Sync successfully');
            //startloader(0);
        } 
    });  
  }   
</script>
