@extends('header')

@section('content')

<div class="container">
    <div class="row">
        <div class="card">            
            <div class="overlay"></div>
            <div class="basic-container dashboardpage">
                <div class="row">
                    <div class="col-md-12">
                        <!--<div class="input-group" id="search_blog">
                            <form method="post" action="{{ URL('product/search') }}" style="display:inherit;">
                                {{ csrf_field() }}
                                <input type="text" class="form-control" placeholder="Search Blog Title" name="search_blog" value="<?php echo isset($_POST['search_blog']) ? $_POST['search_blog'] : ''; ?>">
                                <input type="hidden" name="HDNproductid" value="{{$product_info->product->id}}" >
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    <a href="{{ url('productlist') }}"><button class="btn btn-default back_button" type="button">Back</button></a>
                                </div>                                                                
                            </form>
                        </div>-->                        
                    </div>
                </div>          

                    <!--<form method="POST" id="form_blogSave" action="{{URL('product/store')}}" onsubmit="startloader(1)">-->
                    <!--<form method="POST" id="form_blogSave"> -->
                    {{ csrf_field() }}  
                    <div class="form-group">
                        <label for="producttitle" class="product_detail">Product Title</label> {{ $product_info->product->title }}            
                        <input type="hidden" class="form-control" name="productid" id="productid" value="{{ $product_info->product->id }}">
                        <a href="{{ url('productlist') }}?shop=<?php echo $_REQUEST['shop']; ?>"><button class="btn btn-default back_button" type="button">Back</button></a>
                    </div>
                    <?php if (count($product_info->product->images) > 0) { ?>
                        <div class="form-group">            
                            <label for="producttitle" class="product_detail">Product Image</label>
                            @foreach($product_info->product->images as $image)
                            <img src="{{ $image->src }}" width="80px" style="padding: 5px;border: 1px solid #f8f8f8;" />            
                            @endforeach
                        </div>
                    <?php } ?>                    
                    <input type="hidden" value="<?php echo count($display_blog); ?>" id="blog_count">
                    <div class="row">
                        <div class="col-md-12">                
                            <table id="bloglisttbl" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Select/Unselect Blog</th>
                                        <th>Blog Image</th>
                                        <th>Blog Name</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>                       
                    </div>                    
                    <button type="submit" id="btn_Submit" class="btn btn-primary">Submit</button>
                    <!--</form>-->

            </div>
        </div>    
    </div>
</div>
<script type="text/javascript">
    function startloader(process) {
        if (process == 1) {
            $(".overlay").css({
                'display': 'block',
                'background-image': 'url({{ asset("image/loader.gif") }})',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background-position': 'center'
            });
        } else {
            $(".overlay").css({
                'display': 'none',
                'background-image': 'none',
            });
        }
    }


    $(document).ready(function () {
        var product_ids_data = <?php echo ($blog_productDB != '') ? $blog_productDB : '' ?>;
        
        var selected_products = [];
        if (product_ids_data) {
            var selected_products_array = Object.values(<?php echo $blog_productDB; ?>);
            selected_products = selected_products_array;
        }

        var productid = $('#productid').val();
        var table = $('#bloglisttbl').DataTable({
            "pageLength": 10,
            "lengthChange": false,
            "paging": true,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "{{url('get_bloglist_for_editproducts')}}?shop=<?php echo $_REQUEST['shop']; ?>",
                "type": "POST",
                "data": function (d) {
                    d.productid = productid;
                }
            },
            "cache": false,
            "columns": [
                {
                    "data": "0", "orderable": false
                },
                {
                    "data": "1", "orderable": false,
                    "targets": 0,
                },
                {
                    "render": function (data, type, JsonResultRow, meta) {
                        return '<img src="' + data + '" style="width:50px;height:50px;">';
                    }
                },
                {
                    "data": "3", "orderable": false
                }
            ],
            "select": {
                "style": 'multi',
                "selector": 'td:first-child'
            },
            "order": [[1, 'asc']]
        });
        $(document).ajaxComplete(function () {            
            $(".select-blogcheckbox").click(function () {
                if (jQuery.inArray($(this).val(), selected_products) !== -1) {
                    //console.log("ifclick");
                    var uncheck_productId = selected_products.indexOf($(this).val());
                    selected_products.splice(uncheck_productId, 1);
                    //console.log(selected_products);
                } else {
                    //console.log("elseclick");
                    selected_products.push($(this).val());
                    //console.log(selected_products);
                }
            });
            if(selected_products){
                $(".select-blogcheckbox").each(function() { 
                    var checkIds = jQuery.inArray($(this).val(), selected_products);                    
                    if(checkIds !== -1){
                        $(this).attr('checked', true);
                    }else{
                        $(this).attr('checked', false);
                    }
                });
            }
        });

        /*$("#form_blogSave").on('submit', function (event) {
            saveproduct_ids(event);
        });*/
        $("#btn_Submit").on('click', function (event) {
            startloader(1);
            saveproduct_ids(event);
        });        
        
        function saveproduct_ids(event) {
            var productid = $('#productid').val();
            $.ajax({
                type: "POST",
                url: "{{ url('product/store') }}?shop=<?php echo $_REQUEST['shop']; ?>",
                data: {
                    data: selected_products,
                    productid: productid
                },
                success: function (msg) {
                    toastr.options = {
                      "closeButton": true,
                    }
                    toastr.success("Your blogs are asigned to these products!");
                    var href = "{{ url('productlist') }}?shop=<? echo $_REQUEST['shop']; ?>";
                    window.location.replace(href);
                    //location.reload();
                },
                error: function (jqXHR, exception) {
                    toastr.options = {
                        "closeButton": true,
                    }
                    toastr.error("Could not complete your request, Please try again later.");
                }
            });
        }
    });

</script>
@endsection

