@extends('header')
@section('content')
<?php
$store_name = session('shop');
?>
<div class="container">
    <div class="row">
        <div class="card">

            <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">              
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <img src="" class="imagepreview" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-content help">
                <h5>Need Help?</h5>
                <p><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <ul class="limit">
                    <li>Developer: <b><a target="_blank" href="https://www.zestardshop.com">Zestard Technologies Pvt Ltd</a></b></li>
                    <li>Email: <b><a href="mailto:support@zestard.com">support@zestard.com</a></b></li>
                    <li>Website: <b><a target="_blank" href="https://www.zestardshop.com">https://www.zestardshop.com</a></b></li>
                </ul>
                <hr>

                <h5>General Instruction</h5>
                <h6><b>App Configuration:</b></h6>
                <ul class="limit">
                    <li>After Installing the App, It will show General Settings, where store owner can Configuration the Related Product & Blogs App.</li>
                    <li>On General Settings, Merchant can select the slider type and can add the blog and product slider title.</li>
                    <li>To show blogs slider on product page, merchant can enter number of blogs per row.</li>                    
                    <li>To show product slider on blog page, merchant can enter number of products per row.</li>
                </ul>
                <h6><b>Uninstall App:</b></h6>
                <ul class="limit">
                    <li>To remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                    <li>Click on delete icon of Related Product & Blogs App.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstallApp.png') }}"><b> See Example</b></a></li>
                    <li>If possible then remove shortcode where you have pasted.</li>
                </ul>
                <hr>

                <h5>Follow the Steps:</h5>
                <h6><b>Where to paste shortcode?</b></h6>
                <ul class="limit">						
                    <li>After saving the details, you can copy the <b>Shortcode</b> from the General settings, click on copy to clipboard button.</li>
                    <li>If you want to show blog slider in product page then copy and paste the above shortcode in <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a><a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/product-template.liquid.png') }}"><b> See Example</b></a></li>
                    <li>If you want to show product slider in blog page then copy and paste the above shortcode in <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid</b></a><a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/article-template.liquid.png') }}"><b> See Example</b></a></li>
                    <li>If you want to show random product slider in blog page then copy and paste the above shortcode in <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid.</b></a> If you already pasted the product page shortcode then you can not paste this shortcode. Choose one from product shortcode or random product shortcode.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/article-template.liquid.png') }}"><b> See Example</b></a></li>
                    
                </ul>

                <a class="goback" href="{{ url('generalsettings') }}">
                    <img src="{!! asset('image/back.png') !!}">Go Back
                </a>
            </div>				
        </div>
    </div>
</div>
@endsection
<!--<style> 
    .help h5{
        font-size: 24px;
    }
    .help h6{
        font-size: 16px;
    }
    .help a{
        color: #039be5;
    }
    .help ul{
        padding-left: 0px;
    }
    .limit {
        margin-left: 20px;
    }
    ul.limit li {
        list-style-type: disc !important;
        display: list-item;
    }
</style>-->