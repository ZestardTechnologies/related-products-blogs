@extends('header')

@section('content')
<div class="container">
    <div class="row">
        <div class="card">
            <div class="basic-container dashboardpage">
                <?php //echo "<pre>"; print_r($blogs_result);die; ?>
                <?php //echo "<pre>";print_r($blog_info);die; ?>
                <?php //echo "<pre>";print_r($blogs_array);die; ?>
                <?php //echo "<pre>"; print_r($search_blog); die;   ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group" id="search_blog">
                            <form method="post" action="{{ URL('product/search') }}" style="display:inherit;float:left;margin-right: 10px;">
                                {{ csrf_field() }}
                                <input type="text" class="form-control" placeholder="Search" name="search_blog" value="<?php echo isset($_POST['search_blog']) ? $_POST['search_blog'] : ''; ?>">
                                <input type="hidden" name="HDNproductid" value="{{$product_info->product->id}}" >
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>    
                            </form>
                            <a href="{{URL('edit_product/'.$product_info->product->id)}}"> <button class="btn btn-default" type="button"> Back </button> </a>
                        </div>
                    </div>
                </div>          

                <form method="POST" action="{{URL('product/store')}}">
                    {{ csrf_field() }}      
                    <?php if (count($product_info->product->images) > 0) { ?>
                        <div class="form-group">            
                            <label for="producttitle">Product Image</label> <br> 
                            @foreach($product_info->product->images as $image)
                            <img src="{{ $image->src }}" height="80px" width="80px" style="padding: 5px;border: 1px solid #f8f8f8;" />            
                            @endforeach
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="producttitle">Product Title</label> <br> {{ $product_info->product->title }}            
                        <input type="hidden" class="form-control" name="productid" id="productid" value="{{ $product_info->product->id }}">
                    </div>
                    <input type="hidden" value="<?php echo count($blog_info); ?>" id="blog_count">
                    <div class="row">
                        <div class="col-md-6">                
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Blog Id</th>
                                        <th>Blog Image</th>
                                        <th>Blog Name</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php $i = 1; ?>
                                    <?php $default_image = url('/image/default.png'); ?>
                                    @foreach($blogs_result as $key => $blog)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td style="vertical-align: middle;">{{ $blog->id }}</td>                                         
                                        <td><img src="@if(isset($blog->image)) {{ $blog->image->src }} @else {{ $default_image }} @endif" height="50px;" width="50px;"></td>
                                        <td>{{ $blog->title }}</td>
                                    </tr>
                                    <?php $i++; ?>
                                    @endforeach 

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                </form>

            </div>
        </div>
    </div>
</div>
<!--<script>
$(function () {
    if ($('#blog_count').val() > 5)
    {
        $('#search_blog').show();
    } else
    {
        $('#search_blog').hide();
    }
});
</script>-->
@endsection

