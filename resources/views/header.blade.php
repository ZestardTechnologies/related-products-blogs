@yield('header')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Related Products & Blogs</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">  

        <!-- jquery datatable CSS -->
        <link rel="stylesheet" href="{{ asset('css/datatable/dataTables.bootstrap.min.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.5/css/select.dataTables.min.css">

        <!-- toastr CSS -->
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> 

        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/custom-style.css') }}">

        <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
        <script src="{{ asset('js/datatable/dataTables.bootstrap.min.js') }}"></script>

        <!-- LoadingOverlay start -->    
        <!--<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/src/loadingoverlay.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@1.6.0/extras/loadingoverlay_progress/loadingoverlay_progress.min.js"></script>-->
        <!-- LoadingOverlay end -->

        <!-- shopify Script for fast load -->    
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>

        <script type="text/javascript">
ShopifyApp.init({
apiKey: '606c7e6f0be3efa1f9a2a2c1a0a160e7',
        shopOrigin: '<?php echo "https://" . session('shop'); ?>'
});
ShopifyApp.ready(function () {
ShopifyApp.Bar.initialize({
icon: '',
        title: '',
        buttons: {}
});
});
        </script>
        <?php
        if (!session('shop')) {
            $shop = session('shop');
        } else if(isset($_REQUEST['shop'])) {
            $shop = $_REQUEST['shop'];
        }else{
            $shop = "";
        }        
        ?>
        <script type="text/javascript">
            ShopifyApp.ready(function(){
            ShopifyApp.Bar.initialize({
            buttons: {
            /*primary: {
             label: 'SAVE',
             message: 'form_submit',
             loading: true,
             },*/
            secondary: [{
            label: 'GENERAL SETTINGS',
                    href : "{{ route('generalsettings') }}?shop=<?php echo $shop; ?>",
                    loading: true
            },
            {
            label: 'PRODUCTS',
                    href : '{{ route('productlist') }}?shop=<?php echo $shop; ?>',
                    loading: true
            }, {
            label: 'BLOGS',
                    href : '{{ url('bloglist') }}?shop=<?php echo $shop; ?>',
                    loading: true
            }, {
            label: 'HELP',
                    href : '{{ url('help') }}?shop=<?php echo $shop; ?>',
                    loading: true
            }]
            }
            });
            });
        </script>
    </head>

    <body>

        @yield('navigation')
        <!--<div id="navbar">    
            <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a href="{{ route('generalsettings') }}">General Settings</a></li>
                        <li class="dropdown"><a href="{{ route('productlist') }}">Products</a></li>
                        <li class="dropdown"><a href="{{ url('bloglist') }}">Blog</a></li>
                        <li class="dropdown"><a href="{{ url('help') }}">Help</a></li>
                    </ul>
                </div>
            </nav>
        </div>-->



        @yield('content')

        <script>
            @if (Session::has('notification'))

                    var type = "{{ Session::get('notification.alert-type', 'info') }}";
            toastr.options = {
            "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
            }
            switch (type){
            case 'info':
                    toastr.info("{{ Session::get('notification.message') }}");
            break;
            case 'warning':
                    toastr.warning("{{ Session::get('notification.message') }}");
            break;
            case 'success':
                    toastr.success("{{ Session::get('notification.message') }}");
            break;
            case 'error':
                    toastr.error("{{ Session::get('notification.message') }}");
            break;
            case 'options':
                    toastr.warning("{{ Session::get('notification.message') }}");
            break;
            }
            @endif
        </script>

        <script type="text/javascript">
                    function copyToClipboard(element) {
                    var $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val($(element).text()).select();
                    document.execCommand("copy");
                    //alert('Copied!');
                    $temp.remove();
                    }

            function copyFromDashboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).prev().val()).select();
            document.execCommand("copy");
            //alert('Copied!');
            $temp.remove();
            }
        </script>

        <script>
            jQuery(document).ready(function () {
            jQuery(".copyMe").click(function () {
            var count = jQuery('.show').length;
            if (count == 0) {
            jQuery(".show").show();
            jQuery(".success-copied").after('<div class="alert alert-success alert-dismissable show"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>');
            }
            });
            });
        </script>

        <script type="text/javascript">
            jQuery(function () {
            jQuery('.screenshot').on('click', function () {
            jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
            jQuery('#imagemodal').modal('show');
            });
            });
        </script>

        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>
        <script src="{{ asset('js/javascript.js') }}"></script>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
        <!-- success toasrt message start-->
        <script>
            @if (Session::has('message'))
                    var type = "{{ Session::get('alert-type', 'info') }}";
            switch (type){
            case 'info':
                    toastr.info("{{ Session::get('message') }}");
            break;
            case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
            break;
            case 'success':
                    toastr.success("{{ Session::get('message') }}");
            break;
            case 'error':
                    toastr.error("{{ Session::get('message') }}");
            break;
            }
            @endif
        </script>
        <!-- success toasrt message start-->
    </body>
</html>
