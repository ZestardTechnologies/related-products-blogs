
@extends('header')
@section('content')
<div class="container">
    <div class="row">
        <div class="card">
            <div class="basic-container dashboardpage">    
                <div class="">          
                    <div class="success-copied"></div>                    
                    <div class="col-sm-12">
                        <h2 class="sub-top-heading">Blogs List</h2>
                        <table id="blog_list" class="table table-striped table-bordered" cellspacing="0" width="100%">                
                            <thead>                    
                                <tr>                        
                                    <th style="width:10px !important;">#</th>
                                    <!--<th style="width:10px !important;">Blog Id</th>-->
                                    <th style="width:10px !important;">Blog Image</th>
                                    <th>Blog Title</th>
                                    <th style="width:7px !important;">Assigned Products</th>
                                    <th style="width:7px !important;">Action</th>
                                </tr>                
                            </thead>                                        
                        </table>

                    </div>    
                </div>
            </div>
        </div>    
    </div>
</div>
<script>

    $(document).ready(function () {

    var shop_name = '<?php echo $_REQUEST["shop"]; ?>';
        $('#blog_list').DataTable({
            "pageLength": 10,            
            "paging": true,
            "ordering": true,
            "processing": true,
            "serverSide": true,            
            "ajax": {"url": '{{url('getblogs')}}',"data": 
                        {"shop":shop_name}},
            //"aaData": data,          
            "columns": [
                {
                    "data": "0", "orderable": false
                },
                /*{
                    "data": "1", "orderable": false
                },*/
                {
                    "render": function (data, type, row) {
                        return '<img src="' + data + '" style="width:50px;height:50px;">';
                    }
                },
                {
                    "data": "2", "orderable": false
                },
                {
                    "data": "3", "orderable": false
                },
                {

                    //JsonResultRow, meta => row
                    "render": function (data, type, row) {
                        return '<a href="' + data + '"><span class="glyphicon glyphicon-edit"></span></a>';
                    }
                }],
            "order": [[1, 'asc']]
        });

    });

</script>
@endsection