@extends('header')

@section('content')
    <div class="basic-container dashboardpage">
        <div class="">  
            <div class="success-copied"></div>   	
                <div class="col-sm-12">
                <table id="customer" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px !important;">Customer Id</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Customer Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $customer->customer->id }}</td>
                            <td>{{ $customer->customer->first_name }}</td>
                            <td>{{ $customer->customer->email }}</td>
                            <td>{{ $customer->customer->phone }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="">  
            <div class="success-copied"></div>   	
                <div class="col-sm-12">
                <table id="customer" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px !important;">Customer Id</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Customer Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                </div>
        </div>
    </div>
    <script>
      $(document).ready(function() {
          $('#customers').DataTable({
            "paging":   false,
            "ordering": true,
            "searching": false,
            "bInfo": false
          });
        });
    </script>
@endsection